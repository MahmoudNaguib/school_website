<?php

return[
    'admin\users' => ['edit', 'view', 'delete', 'create'],
    'admin\posts' => ['create', 'edit', 'view', 'delete'],
    'admin\events' => ['create', 'edit', 'view', 'delete'],
    //'admin\options'=>['create', 'edit', 'view', 'delete'],
    'admin\gallery' => ['create', 'edit', 'view', 'delete'],
    'admin\images' => ['create', 'edit', 'view', 'delete'],
    'admin\staff' => ['create', 'edit', 'view', 'delete'],
     'admin\boards' => ['create', 'edit', 'view', 'delete'],
];
