<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider {

    public function boot() {
        view()->composer('front.partials.footer-widget', function($view) {
            $post=new \App\Models\Post;
            $latestPosts=$post->getData()->active()->latest()->take(5)->get();
            $view->with('latestPosts', $latestPosts);
        });
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() {
        //
    }

}
