<?php

namespace App\Providers;

use Config;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\DB;

class MailServiceProvider extends ServiceProvider {

    public function boot() {
        
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register() {
        if (\Schema::hasTable('configs')) {
            $configs = DB::table('configs')->where('type', 'Email Setting')->get();
            $mail = [];
            if ($configs) {
                foreach ($configs as $c) {
                    $key = $c->field;
                    $mail[$key] = $c->value;
                }
                if (@$mail['mail_driver']) {
                    $config = array (
                        'driver' => (@$mail['mail_driver'])?:env('MAIL_DRIVER'),
                        'host' => (@$mail['mail_host'])?:env('MAIL_HOST'),
                        'port' => (@$mail['mail_port'])?:env('MAIL_PORT'),
                        'from' => array ('address' => (@$mail['mail_from_address'])?:env('MAIL_FROM_ADDRESS'), 'name' => (@$mail['mail_from_name'])?:env('MAIL_FROM_NAME')),
                        'encryption' => (@$mail['mail_encryption'])?:env('MAIL_ENCRYPTION'),
                        'username' => (@$mail['mail_username'])?:env('MAIL_USERNAME'),
                        'password' => (@$mail['mail_password'])?:env('MAIL_PASSWORD'),
                    );
                    Config::set('mail', $config);
                }
            }
        }
    }

}
