<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class NotificationsResource extends JsonResource {

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        return [
            'type' => 'notifications',
            'id' => $this->id,
            'attributes' => [
                'from_id' => $this->from_id,
                'to_id' => $this->to_id,
                'message' => $this->message,
                'url' => $this->url,
                'email_notify' => $this->email_notify,
            ],
            'relationships' => [
                'from' => new UserResource($this->whenLoaded('from')),
                'to' => new UserResource($this->whenLoaded('to')),
            ]
        ];
    }

}
