<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class OptionResource extends JsonResource {

    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request) {
        foreach (langs() as $lang) {
            $record[$lang] = $this->getTranslation('title', $lang);
        }
        return [
            'type' => 'options',
            'id' => $this->id,
            'attributes' => [
                'type' => $this->type,
                'title' =>$record,
            ]
        ];
    }

}
