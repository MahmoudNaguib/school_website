<?php

namespace App\Http\Controllers;

class BoardsController extends \App\Http\Controllers\Controller {

    public $module;

    public function __construct(\App\Models\Board $model) {
        $this->module = 'boards';
        $this->views = 'front.' . $this->module;
        $this->model = $model;
    }

    public function getIndex() {
        $data['page_title'] = trans('front.Honor Board');
        $data['meta_description'] = $data['page_title'];
        $data['meta_keywords'] = $data['page_title'];
        $data['image'] = conf('logo');
        $data['rows'] = $this->model->getData()->active()->latest()->paginate(env('PAGE_LIMIT', 10));
        return view($this->views . '.index', $data);
    }

    public function getDetails($id,$slug=NULL) {
        $data['row'] = $this->model->findOrFail($id);
        $data['page_title'] = $data['row']->title;
        $data['meta_description'] = $data['row']->meta_description;
        $data['meta_keywords'] = $data['row']->meta_keywords;
        $data['image'] = $data['row']->image;
        $data['row']->increment('views_count');
        return view($this->views . '.details', $data);
    }

}
