<?php

namespace App\Http\Controllers;

class EventsController extends \App\Http\Controllers\Controller {

    public $module;

    public function __construct(\App\Models\Event $model) {
        $this->module = 'events';
        $this->views = 'front.' . $this->module;
        $this->model = $model;
    }

    public function getIndex() {
        $data['page_title'] = trans('front.Events');
        $data['meta_description'] = $data['page_title'];
        $data['meta_keywords'] = $data['page_title'];
        $data['image'] = conf('logo');
        $data['rows'] = $this->model->EventsCalander();
        $data['colors'] = ["#663090" => 'purple', "#EC1778" => 'pink', "#5B93D3" => 'info', "#23BF08" => 'success', "#f27510" => 'warning'];
        return view($this->views . '.index', $data);
    }

    public function getDetails($id, $slug = NULL) {
        $data['row'] = $this->model->findOrFail($id);
        $data['page_title'] = $data['row']->title;
        $data['meta_description'] = $data['row']->meta_description;
        $data['meta_keywords'] = $data['row']->meta_keywords;
        $data['image'] = $data['row']->image;
         $data['row']->increment('views_count');
        return view($this->views . '.details', $data);
    }

}
