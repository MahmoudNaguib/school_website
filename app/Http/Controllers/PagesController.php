<?php

namespace App\Http\Controllers;

class PagesController extends \App\Http\Controllers\Controller {

    public $module;

    public function __construct() {
        $this->module = 'pages';
        $this->views = 'front.' . $this->module;
    }

    public function about() {
        $data['page_title'] = trans('front.About us');
        $data['meta_description'] = conf('about_meta_description');
        $data['meta_keywords'] = conf('about_meta_keywords');
        $data['image'] = conf('about_banner');
        $data['content'] = conf('about_text');
        return view($this->views . '.about', $data);
    }
    public function contact() {
        $data['page_title'] = trans('front.Contact us');
        $data['meta_description'] = conf('contact_meta_description');
        $data['meta_keywords'] = conf('contact_meta_keywords');
        $data['image'] = conf('contact_banner');
        $data['content'] = conf('contact_text');
        return view($this->views . '.contact', $data);
    }
}
