<?php

namespace App\Http\Controllers\Admin;
use Spatie\Searchable\Search;

class SearchController extends \App\Http\Controllers\Controller {

    public function __construct() {
        $this->module = 'admin/search';
        $this->title = trans('app.Search results');
    }

    public function getIndex() {
        $data['module'] = $this->module;
        $data['page_title'] = $this->title;
        if(request('q')) {
            $data['users'] = \App\Models\User::search(request('q'))->get();
            $data['posts'] = \App\Models\Post::search(request('q'))->get();
            $data['Events'] = \App\Models\Event::search(request('q'))->get();
            $data['gallery'] = \App\Models\Gallery::search(request('q'))->get();
            $data['images'] = \App\Models\Image::search(request('q'))->get();
        }
        return view($this->module.'.index', $data);
    }
}