<?php

namespace App\Http\Controllers\Admin;

class DashBoardController extends \App\Http\Controllers\Controller {

    public $module;

    public function __construct() {
        $this->module = 'admin/dashboard';
    }

    public function getIndex() {
        $data['page_title'] = trans('app.Dashboard');
        return view($this->module . '.index', $data);
    }
}