<?php

namespace App\Http\Controllers;

class HomeController extends \App\Http\Controllers\Controller {

    public $module;

    public function __construct() {
        $this->module = 'home';
        $this->views = 'front.' . $this->module;
    }

    public function getIndex(\App\Models\Post $post, \App\Models\Event $event, \App\Models\Staff $staff, \App\Models\Image $image, \App\Models\Board $board) {
        $data['page_title'] = trans('front.Home');
        $data['latestPosts'] = $post->getData()->active()->latest()->take(2)->get();
        $data['latestEvents'] = $event->getData()->active()->orderBy('date')->take(3)->get();
        $data['latestImages'] = $image->getData()->active()->latest()->take(4)->get();
        $data['staff'] = $staff->getData()->active()->home()->orderBy('id', 'Asc')->take(4)->get();
        $data['boards'] = $board->getData()->active()->orderBy('id', 'Asc')->take(4)->get();
        return view($this->views . '.index', $data);
    }

}
