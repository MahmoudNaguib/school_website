<?php

namespace App\Http\Controllers;

use Auth;
use Hash;
use Validator;

class AuthController extends Controller {

    public $model;
    public $module;

    public function __construct(\App\Models\User $model) {
        $this->middleware('guest');
        $this->module = 'auth';
        $this->model = $model;
        $this->rules = $model->rules;
    }

//    public function getRegister() {
//        $data['page_title'] = trans('auth.Register');
//        $data['module'] = $this->module;
//        return view($this->module . '.register', $data);
//    }
//
//    public function postRegister() {
//        request()->request->add(['confirmed'=>env('USER_CONFIRMATION'),'confirm_token'=>md5(time()).md5(request('email')).md5(rand(1000,10000))]);
//        $this->validate(request(), $this->rules);
//        if ($row = $this->model->create(request()->except(['password_confirmation']))) {
//            flash()->success(trans('app.Account has been created successfully, Please check your email'));
//            return back();
//        }
//        flash()->error(trans('auth.Failed to login'));
//        return back();
//    }

    public function getLogin() {
        $data['page_title'] = trans('auth.Login');
        $data['module'] = $this->module;
        return view($this->module . '.login', $data);
    }

    public function postLogin() {
        $rules = [
            'email' => 'required|email',
            'password' => 'required|min:8',
        ];
        $this->validate(request(), $rules);
        $row = $this->model->where('email', trim(request('email')))->first();
        if (!$row) {
            flash()->error(trans('auth.There is no account with this email'));
            return back()->withInput();
        }
        if (!$row->confirmed) {
            flash()->error(trans('auth.This account is not confirmed'));
            return back()->withInput();
        }
        if (!$row->is_active) {
            flash()->error(trans('auth.This account is banned'));
            return back()->withInput();
        }
        if (!Hash::check(trim(request('password')), $row->password)) {
            flash()->error(trans('auth.Trying to login with invalid password'));
            return back()->withInput();
        }
        if (Auth::attempt(request()->only('email', 'password'), request('remember_me'))) {
            if (request()->has('to')) {
                return redirect(request('to'));
            }
            $row->last_ip = request()->ip();
            $row->last_logged_in_at = date('Y-m-d H:i:s');
            $row->save();
            flash()->success(trans('auth.Welcome to your dashboard'));
            if($row->is_admin)
                return redirect('/' . $row->language.'/admin');
            else
                return redirect('/' . $row->language);
        }
        flash()->error(trans('auth.Failed to login'));
        return back();
    }

    public function getForgotPassword() {
        $data['page_title'] = trans('auth.Forgot your password');
        $data['module'] = $this->module;
        return view($this->module . '.forgot', $data);
    }

    public function postForgotPassword() {
        $rules = [
            'email' => 'required|email',
        ];
        $this->validate(request(), $rules);
        $row = $this->model->where('email', trim(request('email')))->first();
        if (!$row) {
            flash()->error(trans('auth.There is no account with this email'));
            return back()->withInput();
        }
        if (!$row->confirmed) {
            flash()->error(trans('auth.This account is not confirmed'));
            return back()->withInput();
        }
        if (!$row->is_active) {
            flash()->error(trans('auth.This account is banned'));
            return back()->withInput();
        }
        $password = strtolower(str_random(8));
        if ($row->update(['password' => $password])) {
            \App\Jobs\SendForgotEMail::dispatch($row, $password);
            flash()->success(trans('auth.Your new password has been sent to your email'));
            return back();
        }
        flash()->error(trans('auth.Failed to reset password'));
        return back();
    }

    public function getConfirm($token) {
        $row = $this->model->where('confirm_token', '=', $token)->first();
        if (!$row) {
            flash()->error(trans('auth.Invalid user link'));
            return redirect('/');
        }
        $row->confirmed = 1;
        $row->confirm_token = NULL;
        $row->save();
        flash()->success(trans('auth.User has been confirmed'));
        return redirect('/');
    }

}
