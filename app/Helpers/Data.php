<?php

////////////// Default Data
function insertDefaultTokens() {
    $rows = [
        [
            'user_id' => 4,
            'token' => 'sf13bf6d4241be097a975f718adbb179c81e728d9d4c2f636f067f89cc14862c5e33b9abc68925efdc88fe95bf7ac767',
            'expiry_at' => date('Y-m-d', strtotime("+" . env('TOKEN_EXPIRATION', 100) . " days")),
            'device' => 'anoynomous',
            'push_token' => 'anoynomous'
        ]
    ];
    \DB::table('tokens')->insert($rows);
}

function insertDefaultRoles() {
    $rows = [
        [
            'id' => 1,
            'title' => 'Super Administrator',
            'permissions' => json_encode(permissions()),
            'created_by' => 2,
            'is_default' => 1,
        ]
    ];
    \DB::table('roles')->insert($rows);
}

function insertDefaultVendors() {
    foreach (langs() as $lang) {
        $title[$lang] = 'Vendor 1';
    }
    $title = json_encode($title);
    $rows = [
        [
            'id' => 1,
            'title' => $title,
            'created_by' => 2,
        ]
    ];
    \DB::table('vendors')->insert($rows);
}

function insertDefaultUsers() {
    $users = [
        [
            'id' => 1,
            'language' => 'en',
            'confirmed' => 1,
            'is_active' => 1,
            'role_id' => 1,
            'name' => env('SUPER_ADMIN_NAME', 'Super Admin'),
            'email' => env('SUPER_ADMIN_EMAIL', 'super@admin.com'),
            'mobile' => '01221111111',
            'password' => bcrypt('12345678'),
            'image' => generateImage(env('SUPER_ADMIN_NAME', 'Super Admin')),
            'created_by' => 2,
            'is_default' => 1,
        ],
        [
            'id' => 2,
            'language' => 'en',
            'confirmed' => 1,
            'is_active' => 1,
            'role_id' => 1,
            'name' => env('ADMIN_NAME', 'Admin'),
            'email' => env('ADMIN_EMAIL', 'admin@admin.com'),
            'mobile' => '01221111111',
            'password' => bcrypt('12345678'),
            'image' => generateImage(env('ADMIN_NAME', 'Admin')),
            'created_by' => 2,
            'is_default' => 1,
        ]
    ];
    \DB::table('users')->insert($users);
}

function insertDefaultConfigs() {
    $txt_en = "There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don't look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn't anything embarrassing hidden in the middle of text amr songr balga ami toami valo lasi ciri din akr dali";
    $txt_ar = \App\arabicFacker(400);
    \Cache::forget('configs');
    @copy(resource_path() . '/frontend/img/logo.png', public_path() . '/uploads/small/logo_ar.png');
    @copy(resource_path() . '/frontend/img/logo.png', public_path() . '/uploads/large/logo_ar.png');
    @copy(resource_path() . '/frontend/img/logo.png', public_path() . '/uploads/small/logo_en.png');
    @copy(resource_path() . '/frontend/img/logo.png', public_path() . '/uploads/large/logo_en.png');
    @copy(resource_path() . '/frontend/img/home_banner.jpg', public_path() . '/uploads/small/home_banner.jpg');
    @copy(resource_path() . '/frontend/img/home_banner.jpg', public_path() . '/uploads/large/home_banner.jpg');
    @copy(resource_path() . '/frontend/img/vision_banner.jpg', public_path() . '/uploads/small/vision_banner.jpg');
    @copy(resource_path() . '/frontend/img/vision_banner.jpg', public_path() . '/uploads/large/vision_banner.jpg');
    @copy(resource_path() . '/frontend/img/mission_banner.jpg', public_path() . '/uploads/small/mission_banner.jpg');
    @copy(resource_path() . '/frontend/img/mission_banner.jpg', public_path() . '/uploads/large/mission_banner.jpg');
    @copy(resource_path() . '/frontend/img/about_banner.jpg', public_path() . '/uploads/small/about_banner.jpg');
    @copy(resource_path() . '/frontend/img/about_banner.jpg', public_path() . '/uploads/large/about_banner.jpg');
    $rows = [];
    $rows = [];
    //////////// Basic Information
    $txt = env('APP_NAME');
    $row = [
        'field_type' => 'text',
        'field_class' => '',
        'type' => 'General',
        'field' => 'application_name',
        'label' => 'Application Name',
        'value' => 'Al Ferdaws private school',
        'lang' => 'en',
        'created_by' => 2,
    ];
    $rows[] = $row;

    $row = [
        'field_type' => 'text',
        'field_class' => '',
        'type' => 'General',
        'field' => 'application_name',
        'label' => 'Application Name',
        'value' => 'مدرسه الفردوس الخاصه',
        'lang' => 'ar',
        'created_by' => 2,
    ];
    $rows[] = $row;

    $rows[] = [
        'field_type' => 'file',
        'field_class' => 'custom-file-input',
        'type' => 'General',
        'field' => 'logo',
        'label' => 'Logo',
        'value' => 'logo_en.png',
        'lang' => 'en',
        'created_by' => 2,
    ];
    $rows[] = [
        'field_type' => 'file',
        'field_class' => 'custom-file-input',
        'type' => 'General',
        'field' => 'logo',
        'label' => 'logo',
        'value' => 'logo_ar.png',
        'lang' => 'ar',
        'created_by' => 2,
    ];

    //////////// Home Page
    $txt = 'Inspiring the child through practical learning';
    $rows[] = [
        'field_type' => 'text',
        'field_class' => '',
        'type' => 'Home Page',
        'field' => 'home_banner_text',
        'label' => 'Home banner text',
        'value' => $txt,
        'lang' => 'en',
        'created_by' => 2,
    ];
    $txt = 'إلهام الطفل من خلال التعلم العملي';
    $rows[] = [
        'field_type' => 'text',
        'field_class' => '',
        'type' => 'Home Page',
        'field' => 'home_banner_text',
        'label' => 'Home banner text',
        'value' => $txt,
        'lang' => 'ar',
        'created_by' => 2,
    ];
    $rows[] = [
        'field_type' => 'file',
        'field_class' => 'custom-file-input',
        'type' => 'Home Page',
        'field' => 'home_banner',
        'label' => 'Home banner',
        'value' => 'home_banner.jpg',
        'lang' => NULL,
        'created_by' => 2,
    ];
    foreach (langs() as $lang) {
        $row = [
            'field_type' => 'textarea',
            'field_class' => '',
            'type' => 'Home Page',
            'field' => 'vision_text',
            'label' => 'Our vision text',
            'value' => ${'txt_' . $lang},
            'lang' => $lang,
            'created_by' => 2,
        ];
        $rows[] = $row;
    }
    $rows[] = [
        'field_type' => 'file',
        'field_class' => 'custom-file-input',
        'type' => 'Home Page',
        'field' => 'vision_banner',
        'label' => 'Vision banner',
        'value' => 'vision_banner.jpg',
        'lang' => NULL,
        'created_by' => 2,
    ];
    foreach (langs() as $lang) {
        $row = [
            'field_type' => 'textarea',
            'field_class' => '',
            'type' => 'Home Page',
            'field' => 'mission_text',
            'label' => 'Our mission text',
            'value' => ${'txt_' . $lang},
            'lang' => $lang,
            'created_by' => 2,
        ];
        $rows[] = $row;
    }
    $rows[] = [
        'field_type' => 'file',
        'field_class' => 'custom-file-input',
        'type' => 'Home Page',
        'field' => 'mission_banner',
        'label' => 'Mission banner',
        'value' => 'mission_banner.jpg',
        'lang' => NULL,
        'created_by' => 2,
    ];
    foreach (langs() as $lang) {
        $row = [
            'field_type' => 'textarea',
            'field_class' => '',
            'type' => 'Home Page',
            'field' => 'home_meta_description',
            'label' => 'Meta description',
            'value' => '',
            'lang' => $lang,
            'created_by' => 2,
        ];
        $rows[] = $row;
    }
    foreach (langs() as $lang) {
        $row = [
            'field_type' => 'text',
            'field_class' => 'tags',
            'type' => 'Home Page',
            'field' => 'home_meta_keywords',
            'label' => 'Meta keywords',
            'value' => '',
            'lang' => $lang,
            'created_by' => 2,
        ];
        $rows[] = $row;
    }


    ///////////////////////// About us
    foreach (langs() as $lang) {
        $row = [
            'field_type' => 'textarea',
            'field_class' => 'editor',
            'type' => 'About Us',
            'field' => 'about_text',
            'label' => 'About us',
            'value' => ${'txt_' . $lang},
            'lang' => $lang,
            'created_by' => 2,
        ];
        $rows[] = $row;
    }
    $rows[] = [
        'field_type' => 'file',
        'field_class' => 'custom-file-input',
        'type' => 'About Us',
        'field' => 'about_banner',
        'label' => 'Banner',
        'value' => 'about_banner.jpg',
        'lang' => NULL,
        'created_by' => 2,
    ];
    foreach (langs() as $lang) {
        $row = [
            'field_type' => 'textarea',
            'field_class' => '',
            'type' => 'About Us',
            'field' => 'about_meta_description',
            'label' => 'Meta description',
            'value' => '',
            'lang' => $lang,
            'created_by' => 2,
        ];
        $rows[] = $row;
    }
    foreach (langs() as $lang) {
        $row = [
            'field_type' => 'text',
            'field_class' => 'tags',
            'type' => 'About Us',
            'field' => 'about_meta_keywords',
            'label' => 'Meta keywords',
            'value' => '',
            'lang' => $lang,
            'created_by' => 2,
        ];
        $rows[] = $row;
    }
    ///////////////////////// Contact
    $txt_en = 'We are happy to receive your message';
    $txt_ar = 'نحن سعداء باتصالك معنا';
    foreach (langs() as $lang) {
        $row = [
            'field_type' => 'textarea',
            'field_class' => 'editor',
            'type' => 'Contact us',
            'field' => 'contact_text',
            'label' => 'Contact us text',
            'value' => ${'txt_' . $lang},
            'lang' => $lang,
            'created_by' => 2,
        ];
        $rows[] = $row;
    }
    $rows[] = [
        'field_type' => 'file',
        'field_class' => 'custom-file-input',
        'type' => 'Contact us',
        'field' => 'contact_banner',
        'label' => 'Banner',
        'value' => 'about_banner.jpg',
        'lang' => NULL,
        'created_by' => 2,
    ];
    foreach (langs() as $lang) {
        $row = [
            'field_type' => 'textarea',
            'field_class' => '',
            'type' => 'Contact us',
            'field' => 'contact_meta_description',
            'label' => 'Meta description',
            'value' => '',
            'lang' => $lang,
            'created_by' => 2,
        ];
        $rows[] = $row;
    }
    foreach (langs() as $lang) {
        $row = [
            'field_type' => 'text',
            'field_class' => 'tags',
            'type' => 'Contact us',
            'field' => 'contact_meta_keywords',
            'label' => 'Meta keywords',
            'value' => '',
            'lang' => $lang,
            'created_by' => 2,
        ];
        $rows[] = $row;
    }
    ///////////////// Contact information
    $rows[] = [
        'field_type' => 'text',
        'field_class' => '',
        'type' => 'Contact Information',
        'field' => 'email',
        'label' => 'Email',
        'value' => env('CONTACT_EMAIL', 'contact@alferdaws.com'),
        'lang' => NULL,
        'created_by' => 2,
    ];
    $rows[] = [
        'field_type' => 'text',
        'field_class' => '',
        'type' => 'Contact Information',
        'field' => 'phone',
        'label' => 'Phone',
        'value' => '12345678',
        'lang' => NULL,
        'created_by' => 2,
    ];
    $rows[] = [
        'field_type' => 'text',
        'field_class' => '',
        'type' => 'Contact Information',
        'field' => 'mobile',
        'label' => 'Mobile',
        'value' => '12345678',
        'lang' => NULL,
        'created_by' => 2,
    ];
    $txt_en = "1 Al Ouroba st, Corniche El Nile";
    $txt_ar = "1 شارع العروبه , كورنيش النيل";
    foreach (langs() as $lang) {
        $row = [
            'field_type' => 'textarea',
            'field_class' => '',
            'type' => 'Contact Information',
            'field' => 'address',
            'label' => 'Address',
            'value' => ${'txt_' . $lang},
            'lang' => $lang,
            'created_by' => 2,
        ];
        $rows[] = $row;
    }
    ///////////////// longitude
    $rows[] = [
        'field_type' => 'text',
        'field_class' => '',
        'type' => 'Contact Information',
        'field' => 'longitude',
        'label' => 'Location (longitude)',
        'value' => '31.2355891',
        'lang' => NULL,
        'created_by' => 2,
    ];
    ///////////////// latitude
    $rows[] = [
        'field_type' => 'text',
        'field_class' => '',
        'type' => 'Contact Information',
        'field' => 'latitude',
        'label' => 'Location (latitude)',
        'value' => '29.9786832',
        'lang' => NULL,
        'created_by' => 2,
    ];
    $rows[] = [
        'field_type' => 'text',
        'field_class' => '',
        'type' => 'Contact Information',
        'field' => 'map_url',
        'label' => 'Map url',
        'value' => 'https://goo.gl/maps/cJXRmpoCUS66PutW7',
        'lang' => NULL,
        'created_by' => 2,
    ];
    $rows[] = [
        'field_type' => 'text',
        'field_class' => '',
        'type' => 'Contact Information',
        'field' => 'contact_submit_email',
        'label' => 'Contact submit email',
        'value' => 'contact@school.com',
        'lang' => NULL,
        'created_by' => 2,
    ];
    ///////////////// Social Links 
    $rows[] = [
        'field_type' => 'text',
        'field_class' => '',
        'type' => 'Social Links',
        'field' => 'facebook_link',
        'label' => 'Facebook link',
        'value' => 'http://www.facebook.com/alferdaws',
        'lang' => NULL,
        'created_by' => 2,
    ];
    $rows[] = [
        'field_type' => 'text',
        'field_class' => '',
        'type' => 'Social Links',
        'field' => 'instagram_link',
        'label' => 'Instagram link',
        'value' => 'http://www.instagram.com/alferdaws',
        'lang' => NULL,
        'created_by' => 2,
    ];
    $rows[] = [
        'field_type' => 'text',
        'field_class' => '',
        'type' => 'Social Links',
        'field' => 'twitter_link',
        'label' => 'Twitter link',
        'value' => 'http://www.twitter.com/alferdaws',
        'lang' => NULL,
        'created_by' => 2,
    ];
    $rows[] = [
        'field_type' => 'text',
        'field_class' => '',
        'type' => 'Social Links',
        'field' => 'youtube_link',
        'label' => 'Youtube link',
        'value' => 'http://www.youtube.com/alferdaws',
        'lang' => NULL,
        'created_by' => 2,
    ];


    ///////////////// Email Setting 
    $rows[] = [
        'field_type' => 'text',
        'field_class' => '',
        'type' => 'Email Setting',
        'field' => 'mail_driver',
        'label' => 'Mail Driver',
        'value' => env('MAIL_DRIVER'),
        'lang' => NULL,
        'created_by' => 2,
    ];
    $rows[] = [
        'field_type' => 'text',
        'field_class' => '',
        'type' => 'Email Setting',
        'field' => 'mail_host',
        'label' => 'Mail Host',
        'value' => env('MAIL_HOST'),
        'lang' => NULL,
        'created_by' => 2,
    ];
    $rows[] = [
        'field_type' => 'text',
        'field_class' => '',
        'type' => 'Email Setting',
        'field' => 'mail_port',
        'label' => 'Mail Port',
        'value' => env('MAIL_PORT'),
        'lang' => NULL,
        'created_by' => 2,
    ];
    $rows[] = [
        'field_type' => 'text',
        'field_class' => '',
        'type' => 'Email Setting',
        'field' => 'mail_username',
        'label' => 'Mail Username',
        'value' => env('MAIL_USERNAME'),
        'lang' => NULL,
        'created_by' => 2,
    ];
    $rows[] = [
        'field_type' => 'text',
        'field_class' => '',
        'type' => 'Email Setting',
        'field' => 'mail_password',
        'label' => 'Mail Password',
        'value' => env('MAIL_PASSWORD'),
        'lang' => NULL,
        'created_by' => 2,
    ];
    $rows[] = [
        'field_type' => 'text',
        'field_class' => '',
        'type' => 'Email Setting',
        'field' => 'mail_encryption',
        'label' => 'Mail Encryption',
        'value' => env('MAIL_ENCRYPTION'),
        'lang' => NULL,
        'created_by' => 2,
    ];
    $rows[] = [
        'field_type' => 'text',
        'field_class' => '',
        'type' => 'Email Setting',
        'field' => 'mail_from_address',
        'label' => 'Mail from address',
        'value' => env('MAIL_FROM_ADDRESS'),
        'lang' => NULL,
        'created_by' => 2,
    ];
    $rows[] = [
        'field_type' => 'text',
        'field_class' => '',
        'type' => 'Email Setting',
        'field' => 'mail_from_name',
        'label' => 'Mail From Name',
        'value' => env('MAIL_FROM_NAME'),
        'lang' => NULL,
        'created_by' => 2,
    ];
    \DB::table('configs')->insert($rows);
}

function insertDefaultOptions() {
    $rows = [
        [
            'type' => 'levels',
            'title' => 'Beginner',
            'created_by' => 2,
            'is_default' => 1
        ],
        [
            'type' => 'levels',
            'title' => 'Intermediate',
            'created_by' => 2,
            'is_default' => 1
        ],
        [
            'type' => 'levels',
            'title' => 'Advanced',
            'created_by' => 2,
            'is_default' => 1
        ],
        [
            'type' => 'languages',
            'title' => 'Arabic',
            'created_by' => 2,
            'is_default' => 1
        ],
        [
            'type' => 'languages',
            'title' => 'English',
            'created_by' => 2,
            'is_default' => 1
        ],
        [
            'type' => 'languages',
            'title' => 'French',
            'created_by' => 2,
            'is_default' => 1
        ],
    ];
    if ($rows) {
        foreach ($rows as &$row) {
            foreach (langs() as $lang) {
                $title[$lang] = $row['title'];
            }
            $row['title'] = json_encode($title);
        }
    }
    Illuminate\Support\Facades\DB::table('options')->insert($rows);
}
