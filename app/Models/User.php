<?php

namespace App\Models;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable {

    use SoftDeletes,
        \App\Models\Traits\CreatedBy,
        \App\Models\Traits\HasAttach,
        \Laravel\Scout\Searchable,
        \Spatie\Translatable\HasTranslations;

    protected $table = "users";
    protected $guarded = [
        'deleted_at',
        'logged_user',
        'image',
        'teams',
    ];
    protected $hidden = [
        'password',
        'remember_token',
        'confirm_token',
        'confirmed',
        'is_active',
        'is_admin',
        'created_by',
        'updated_at',
        'deleted_at',
        'language'
    ];
    protected $appends = ['is_admin', 'is_super_admin'];
    public $translatable = ['bio','meta_description', 'meta_keywords'];
    ///////////////////////////// has attach
    protected static $attachFields = [
        'image' => [
            'sizes' => ['small' => 'crop,100x100', 'large' => 'resize,400x400'],
            'path' => 'uploads'
        ],
    ];
    public $rules = [
        'name' => 'required',
        'email' => 'required|email|unique:users,email',
        'mobile' => 'required|mobile',
        'password' => "required|confirmed|min:8",
        'image' => 'nullable|image|max:4000'
    ];

    public function toSearchableArray() {
        $array = [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'mobile' => $this->mobile,
        ];
        return $array;
    }

    public static function boot() {
        parent::boot();
        static::created(function ($row) {
            if (!request()->hasFile('image') && !$row->image) {
                $image = generateImage($row->name);
                \DB::table('users')->where('id', $row->id)->update(['image' => $image]);
            }
            \App\Jobs\SendRegisterEMail::dispatch($row);
        });
    }

    public function getIsAdminAttribute() {
        return ($this->role_id);
    }

    public function getIsSuperAdminAttribute() {
        return ($this->role_id == 1);
    }

    public function notifications() {
        return $this->hasMany(Notification::class, 'to_id');
    }

    public function role() {
        return $this->belongsTo(Role::class, 'role_id')->withTrashed()->withDefault();
    }

    public function tokens() {
        return $this->hasMany(Token::class);
    }

    public function getGenders() {
        return \App\Models\Option::where('type', 'gender')->pluck('title', 'id');
    }

    public function getRoles() {
        return \App\Models\Role::pluck('title', 'id');
    }

    public function setPasswordAttribute($value) {
        if (trim($value)) {
            $this->attributes['password'] = bcrypt(trim($value));
        }
    }

    public function scopeActive($query) {
        return $query->where('is_active', '=', 1)->where('confirmed', 1);
    }

    public function getData() {
        return $this->with(['role', 'creator']);
    }

    public function export($rows, $fileName) {
        if ($rows) {
            foreach ($rows as $row) {
                unset($object);
                $object['id'] = $row->id;
                $object['Name'] = $row->name;
                $object['Email'] = $row->email;
                $object['Mobile'] = $row->mobile;
                $object['Is Admin'] = ($row->is_admin) ? 'Yes' : 'No';
                $object['Created at'] = $row->created_at;
                $labels = array_keys($object);
                $data[] = $object;
            }
            export($data, $labels, $fileName);
        }
    }

}
