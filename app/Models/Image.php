<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon;

class Image extends \App\Models\BaseModel {

    use SoftDeletes,
        \App\Models\Traits\CreatedBy,
        \App\Models\Traits\HasAttach,
        \Laravel\Scout\Searchable,
        \Spatie\Translatable\HasTranslations;

    ///////////////////////////// has translation
    public $translatable = ['title'];
    protected $table = "images";
    protected $guarded = [
        'deleted_at',
        'image',
        'logged_user'
    ];
    protected $hidden = [
        'deleted_at',
    ];
    protected static $attachFields = [
        'image' => [
            'sizes' => ['small' => 'crop,236x234', 'large' => 'resize,870x439'],
            'path' => 'uploads'
        ],
    ];
    public $rules = [
        'imageable_id' => 'required',
        'imageable_type' => 'required',
        'title' => 'required',
        'image' => 'nullable|image|max:4000'
    ];

    public function toSearchableArray() {
        $array = [
            'id' => $this->id,
            'title' => $this->title,
        ];
        return $array;
    }

    public static function boot() {
        parent::boot();
        static::created(function ($row) {
            if (!request()->hasFile('image') && !$row->image) {
                $image = generateImage($row->title, ['small'=>'240x180','large'=>'720x540']);
                $data['image'] = $image;
                \DB::table('images')->where('id', $row->id)->update($data);
            }
        });
    }

    public function getGalleries() {
        return \App\Models\Gallery::pluck('title', 'id');
    }

    public function imageable() {
        return $this->morphTo('imageable')->withTrashed()->withDefault();
    }

    public function scopeActive($query) {
        return $query->where('is_active', '=', 1);
    }

    public function getData() {
        return $this->with(['imageable'])
                        ->when(request('imageable_id'), function($q) {
                            return $q->where('imageable_id', request('imageable_id'));
                        })
                        ->when(request('imageable_type'), function($q) {
                            return $q->where('imageable_type', request('imageable_type'));
                        });
    }

    public function export($rows, $fileName) {
        if ($rows) {
            foreach ($rows as $row) {
                unset($object);
                $object['id'] = $row->id;
                $object['Gallery'] = $row->gallery->title;
                $object['Title'] = $row->title;
                $object['Is Active'] = ($row->is_active) ? trans('app.Yes') : trans('app.No');
                $object['Created at'] = $row->created_at;
                $labels = array_keys($object);
                $data[] = $object;
            }
            export($data, $labels, $fileName);
        }
    }

    public function getLinkAttribute() {
        return lang() . '/images/details/' . $this->id . '/' . $this->slug;
    }

    public function getTitleLimitedAttribute() {
        return str_limit($this->title, 50);
    }

    public function getContentLimitedAttribute() {
        return str_limit(strip_tags($this->content), 70);
    }

}
