<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon;

class Staff extends \App\Models\BaseModel {

    use SoftDeletes,
        \App\Models\Traits\CreatedBy,
        \App\Models\Traits\HasAttach,
        \Laravel\Scout\Searchable,
        \Spatie\Translatable\HasTranslations;

    ///////////////////////////// has translation
    public $translatable = ['name', 'slug', 'content', 'meta_description', 'meta_keywords'];
    protected $table = "staff";
    protected $guarded = [
        'deleted_at',
        'image',
        'logged_user'
    ];
    protected $hidden = [
        'deleted_at',
    ];
    protected static $attachFields = [
        'image' => [
            'sizes' => ['small' => 'crop,200x200', 'large' => 'resize,400x400'],
            'path' => 'uploads'
        ],
    ];
    public $rules = [
        'job_title_id' => 'required',
        'name' => 'required',
        'slug' => 'required',
        'content' => 'required',
        'image' => 'nullable|image|max:4000'
    ];

    public function toSearchableArray() {
        $array = [
            'id' => $this->id,
            'name' => $this->name,
            'content' => $this->content,
        ];
        return $array;
    }

    public function getJobTitles() {
        return \App\Models\JobTitle::active()->pluck('title', 'id');
    }

    public function job_title() {
        return $this->belongsTo(JobTitle::class, 'job_title_id')->withTrashed()->withDefault();
    }

    public static function boot() {
        parent::boot();
        static::created(function ($row) {
            if (!request()->hasFile('image') && !$row->image) {
                $image = generateImage('Staff Image', ['small'=>'200x200','large'=>'400x400']);
                $data['image'] = $image;
                \DB::table('staff')->where('id', $row->id)->update($data);
            }
        });
    }

    public function scopeActive($query) {
        return $query->where('is_active', '=', 1);
    }

    public function scopeHome($query) {
        return $query->where('show_in_home_page', '=', 1);
    }

    public function getData() {
        return $this->with(['job_title']);
    }

    public function export($rows, $fileName) {
        if ($rows) {
            foreach ($rows as $row) {
                unset($object);
                $object['id'] = $row->id;
                $object['Job title'] = $row->job_title->title;
                $object['Name'] = $row->name;
                $object['Content'] = $row->content;
                $object['Is Active'] = ($row->is_active) ? trans('app.Yes') : trans('app.No');
                $object['Created at'] = $row->created_at;
                $labels = array_keys($object);
                $data[] = $object;
            }
            export($data, $labels, $fileName);
        }
    }

    public function getLinkAttribute() {
        return lang() . '/staff/details/' . $this->id . '/' . $this->slug;
    }

    public function getNameLimitedAttribute() {
        return str_limit($this->name, 35);
    }

    public function getContentLimitedAttribute() {
        return str_limit(strip_tags($this->content), 60);
    }

}
