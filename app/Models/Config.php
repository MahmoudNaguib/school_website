<?php

namespace App\Models;

class Config extends BaseModel {

    use \App\Models\Traits\CreatedBy;

    protected $table = "configs";
    protected $guarded = [
        'logged_user'
    ];
    protected $hidden = [
    ];
    public $rules = [
        'type' => 'required',
        'field' => 'required',
    ];

}
