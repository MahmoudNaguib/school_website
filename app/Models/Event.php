<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Carbon;

class Event extends \App\Models\BaseModel {

    use SoftDeletes,
        \App\Models\Traits\CreatedBy,
        \App\Models\Traits\HasAttach,
        \Laravel\Scout\Searchable,
        \Spatie\Translatable\HasTranslations;

    ///////////////////////////// has translation
    public $translatable = ['title', 'tags', 'slug', 'content', 'meta_description', 'meta_keywords'];
    protected $table = "events";
    protected $guarded = [
        'deleted_at',
        'image',
        'logged_user'
    ];
    protected $hidden = [
        'deleted_at',
    ];
    protected static $attachFields = [
        'image' => [
            'sizes' => ['small' => 'crop,240x180', 'large' => 'resize,720x540'],
            'path' => 'uploads'
        ],
    ];
    public $rules = [
        'title' => 'required',
        'slug' => 'required',
        'content' => 'required',
        'image' => 'nullable|image|max:4000'
    ];

    public function toSearchableArray() {
        $array = [
            'id' => $this->id,
            'title' => $this->title,
            'tags' => $this->tags,
            'content' => $this->content,
        ];
        return $array;
    }

    public static function boot() {
        parent::boot();
        static::created(function ($row) {
            if (!request()->hasFile('image') && !$row->image) {
                $image = generateImage('Events Image', ['small' => '240x180', 'large' => '720x540']);
                $data['image'] = $image;
                \DB::table('events')->where('id', $row->id)->update($data);
            }
        });
    }

    public function scopeActive($query) {
        return $query->where('is_active', '=', 1);
    }

    public function getData() {
        return $this;
    }

    public function export($rows, $fileName) {
        if ($rows) {
            foreach ($rows as $row) {
                unset($object);
                $object['id'] = $row->id;
                $object['Title'] = $row->title;
                $object['Content'] = $row->content;
                $object['Tags'] = $row->tags;
                $object['Is Active'] = ($row->is_active) ? trans('app.Yes') : trans('app.No');
                $object['Created at'] = $row->created_at;
                $labels = array_keys($object);
                $data[] = $object;
            }
            export($data, $labels, $fileName);
        }
    }

    public function getLinkAttribute() {
        return lang() . '/events/details/' . $this->id . '/' . $this->slug;
    }

    public function getTitleLimitedAttribute() {
        return str_limit($this->title, 35);
    }

    public function getContentLimitedAttribute() {
        return str_limit(strip_tags($this->content), 60);
    }

    public function EventsCalander() {
        $url = 'events/details/';
        $data = $this->getData()->where('date', '>=', date('Y-m-d'))->latest()->get();
        $events = [];
        if ($data->count()) {
            foreach ($data as $key => $value) {
                $events[] = \Calendar::event(
                                $value->title . ' : ' . @$value->time, true, new \DateTime($value->date), new \DateTime($value->date . ' +1 day'), null,
                                // Add color and link on event
                                [
                            'color' => '#1b84e7',
                            'url' => $url . $value->id,
                                ]
                );
            }
        }
        return \Calendar::addEvents($events);
    }

}
