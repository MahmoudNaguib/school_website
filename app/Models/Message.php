<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Message extends BaseModel {

    use SoftDeletes,
        \Laravel\Scout\Searchable;

    protected $table = "messages";
    protected $guarded = [
        'deleted_at',
        'logged_user',
    ];
    protected $hidden = [
        'deleted_at',
    ];
    public $rules = [
        'name' => 'required',
        'mobile' => 'required',
        'email' => 'required|email',
        'content' => 'required',
    ];

    public static function boot() {
        parent::boot();
        static::created(function ($row) {
            \App\Jobs\SendContactUsToAdmin::dispatch($row);
        });
    }

    public function toSearchableArray() {
        $array = [
            'id' => $this->id,
            'name' => $this->name,
            'email' => $this->email,
            'content' => $this->content,
        ];
        return $array;
    }

    public function getData() {
        return $this;
    }

    public function export($rows, $fileName) {
        if ($rows) {
            foreach ($rows as $row) {
                unset($object);
                $object['id'] = $row->id;
                $object['Name'] = $row->name;
                $object['Mobile'] = $row->name;
                $object['Email'] = $row->email;
                $object['Content'] = $row->content;
                $object['Created at'] = $row->created_at;
                $labels = array_keys($object);
                $data[] = $object;
            }
            export($data, $labels, $fileName);
        }
    }

}
