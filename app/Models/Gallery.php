<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;

class Gallery extends BaseModel {

    use SoftDeletes,
        \App\Models\Traits\CreatedBy,
        \App\Models\Traits\HasAttach,
        \Laravel\Scout\Searchable,
        \Spatie\Translatable\HasTranslations;

    ///////////////////////////// has translation
    public $translatable = ['title','slug'];
    protected $table = "gallery";
    protected $guarded = [
        'deleted_at',
        'image',
        'logged_user'
    ];
    protected $hidden = [
        'deleted_at',
    ];
    public $rules = [
        'title' => 'required',
    ];
    protected static $attachFields = [
        'image' => [
            'sizes' => ['small' => 'crop,240x180', 'large' => 'resize,720x540'],
            'path' => 'uploads'
        ],
    ];

    public function toSearchableArray() {
        $array = [
            'id' => $this->id,
            'title' => $this->title,
        ];
        return $array;
    }

    public static function boot() {
        parent::boot();
        static::created(function ($row) {
            if (!request()->hasFile('image') && !$row->image) {
                $image = generateImage('Gallery Image', ['small' => '240x180', 'large' => '720x540']);
                $data['image'] = $image;
                \DB::table('gallery')->where('id', $row->id)->update($data);
            }
        });
    }

    public function getData() {
        return $this->with(['images']);
    }

    public function images() {
        return $this->morphMany(Image::class, 'imageable');
    }

    public function export($rows, $fileName) {
        if ($rows) {
            foreach ($rows as $row) {
                unset($object);
                $object['id'] = $row->id;
                $object['Title'] = @$row->title;
                $object['Created at'] = $row->created_at;
                $labels = array_keys($object);
                $data[] = $object;
            }
            export($data, $labels, $fileName);
        }
    }

    public function scopeActive($query) {
        return $query->where('is_active', '=', 1);
    }

    public function getLinkAttribute() {
        return lang() . '/gallery/details/' . $this->id . '/' . $this->slug;
    }

}
