<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class CreateMonthlyInvoices extends Command {

    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'invoices:monthly_create';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create monthly invoices for recurring invoices';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct() {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle() {
        if (env('ENABLE_MONEY')) {
            $invoices = \App\Models\Money\Invoice::where('is_recurring', 1)->where('date', date("Y-m-d", strtotime("-30 day")))->where('period', 'weekly')->get();
            if ($invoices) {
                foreach ($invoices as $row) {
                    $clone = $row->replicate();
                    $clone->date = date('Y-m-d', strtotime($clone->date . ' +30 days'));
                    $clone->due_date = date('Y-m-d', strtotime($clone->due_date . ' +30 days'));
                    if ($clone->attachment) {
                        // dd($clone->attachment);
                        $new_attachments = rand(1, 1000) . $clone->attachment;
                        @copy(public_path() . '/uploads/' . $clone->attachment, public_path() . '/uploads/' . $new_attachments);
                        $clone->attachment = $new_attachments;
                    }
                    $clone->status = 98;
                    $clone->save();
                    foreach ($row->items as $item) {
                        $clone_item = $item->replicate();
                        $clone_item->invoice_id = $clone->id;
                        $clone_item->save();
                    }
                    $row->is_recurring = NUll;
                    $row->period = NULL;
                    $row->save();
                }
            }
        }
    }

}
