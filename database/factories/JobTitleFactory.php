<?php
use Faker\Generator as Faker;
/*
  |--------------------------------------------------------------------------
  | Model Factories
  |--------------------------------------------------------------------------
  |
  | This directory should contain each of the model factory definitions for
  | your application. Factories provide a convenient way to generate new
  | model instances for testing / seeding your application's database.
  |
 */

$factory->define(App\Models\JobTitle::class, function (Faker $faker) {
    $record="Job title ".rand(1,1000);
    foreach(langs() as $lang) {
       if ($lang == 'ar')
            $faker = new \App\ArabicFaker;
        $title[$lang] = $faker->text(40);
    }
    return [
        'title'=>$title,
        'created_by'=>2,
        'is_active'=>1
    ];
});
