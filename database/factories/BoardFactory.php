<?php

use Faker\Generator as Faker;

/*
  |--------------------------------------------------------------------------
  | Model Factories
  |--------------------------------------------------------------------------
  |
  | This directory should contain each of the model factory definitions for
  | your application. Factories provide a convenient way to generate new
  | model instances for testing / seeding your application's database.
  |
 */
$factory->define(App\Models\Board::class, function (Faker $faker) { 
    foreach (langs() as $lang) {
        $title[$lang] =  $name[$lang]=$faker->firstName.' '.$faker->lastName;
        $slug[$lang] = slug($title[$lang]);
        $content[$lang] = $faker->text(40);
    }
    return [
        'title' => $title,
        'slug' => $slug,
        'content' => $content,
        'is_active' => 1,
        'created_by' => 2,
        'views_count' => rand(1, 1000)
    ];
});
