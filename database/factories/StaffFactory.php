<?php
use Faker\Generator as Faker;
/*
  |--------------------------------------------------------------------------
  | Model Factories
  |--------------------------------------------------------------------------
  |
  | This directory should contain each of the model factory definitions for
  | your application. Factories provide a convenient way to generate new
  | model instances for testing / seeding your application's database.
  |
 */

$factory->define(App\Models\Staff::class, function (Faker $faker) {
    foreach(langs() as $lang) {
        $name[$lang]=$faker->firstName.' '.$faker->lastName;
        $slug[$lang] = slug($name[$lang]);
        $content[$lang] = $faker->text(300);
    }
    $Ids = App\Models\JobTitle::pluck('id')->toArray();
    return [
        'job_title_id' => $Ids[array_rand($Ids)],
        'name' => $name,
        'slug' => $slug,
        'content'=>$content,
        'created_by'=>2,
        'is_active'=>1,
        'show_in_home_page'=>1
    ];
});
