<?php

use Faker\Generator as Faker;

/*
  |--------------------------------------------------------------------------
  | Model Factories
  |--------------------------------------------------------------------------
  |
  | This directory should contain each of the model factory definitions for
  | your application. Factories provide a convenient way to generate new
  | model instances for testing / seeding your application's database.
  |
 */

$factory->define(App\Models\Image::class, function (Faker $faker) {
    foreach (langs() as $lang) {
        if ($lang == 'ar')
            $faker = new \App\ArabicFaker;
        $title[$lang] = $faker->text(35);
    }
    $galleryIds = App\Models\Gallery::pluck('id')->toArray();
    return [
        'imageable_id' => $galleryIds[array_rand($galleryIds)],
        'imageable_type'=>'App\Models\Gallery',
        'title' => $title,
        'is_active' => 1,
        'created_by' => 2,
    ];
});
