<?php

use Faker\Generator as Faker;

/*
  |--------------------------------------------------------------------------
  | Model Factories
  |--------------------------------------------------------------------------
  |
  | This directory should contain each of the model factory definitions for
  | your application. Factories provide a convenient way to generate new
  | model instances for testing / seeding your application's database.
  |
 */

$factory->define(App\Models\Event::class, function (Faker $faker) {
    foreach (langs() as $lang) {
        if ($lang == 'ar')
            $faker = new \App\ArabicFaker;
        $title[$lang] = $faker->text(50);
        $slug[$lang] = slug($title[$lang]);
        $content[$lang] = $faker->text(300);
    }
    return [
        'title' => $title,
        'slug' => $slug,
        'content' => $content,
        'date' => date('Y-m-d', strtotime(date("Y-m-d", strtotime("tomorrow")) . ' + ' . rand(1, 30) . ' days')),
        'time' => '0' . rand(1, 9) . ':00 AM',
        'is_active' => 1,
        'created_by' => 2,
        'views_count' => rand(1, 1000)
    ];
});
