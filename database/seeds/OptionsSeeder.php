<?php

use Illuminate\Database\Seeder;

class OptionsSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if (app()->environment() != 'production') {
            DB::table('options')->delete();
            DB::statement("ALTER TABLE options AUTO_INCREMENT = 1");
            insertDefaultOptions();
            //factory(App\Models\Option::class, 10)->create();
        }
    }

}
