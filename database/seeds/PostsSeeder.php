<?php
use Illuminate\Database\Seeder;

class PostsSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if(app()->environment()!='production') {
            DB::table('posts')->delete();
            DB::statement("ALTER TABLE posts AUTO_INCREMENT = 1");
            factory(App\Models\Post::class, 10)->create();
        }
    }
}