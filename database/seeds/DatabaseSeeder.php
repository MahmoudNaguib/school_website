<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {

    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run() {
        $this->call(BasicSeeder::class);
        if (app()->environment() != 'production') {
            $this->call(NotificationsSeeder::class);
            $this->call(PostsSeeder::class);
            $this->call(EventsSeeder::class);
            $this->call(GallerySeeder::class);
            $this->call(ImagesSeeder::class);
            $this->call(JobTitlesSeeder::class);
            $this->call(StaffSeeder::class);
            $this->call(BoardsSeeder::class);
        }
    }

}
