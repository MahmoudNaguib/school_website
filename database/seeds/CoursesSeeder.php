<?php
use Illuminate\Database\Seeder;

class CoursesSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if(app()->environment()!='production') {
            DB::table('courses')->delete();
            DB::statement("ALTER TABLE courses AUTO_INCREMENT = 1");
            factory(App\Models\Course::class, 25)->create();
        }
    }
}