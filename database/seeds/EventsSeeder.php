<?php
use Illuminate\Database\Seeder;

class EventsSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if(app()->environment()!='production') {
            DB::table('events')->delete();
            DB::statement("ALTER TABLE events AUTO_INCREMENT = 1");
            factory(App\Models\Event::class, 10)->create();
        }
    }
}