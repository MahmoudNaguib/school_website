<?php
use Illuminate\Database\Seeder;

class ImagesSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if(app()->environment()!='production') {
            DB::table('images')->delete();
            DB::statement("ALTER TABLE images AUTO_INCREMENT = 1");
            factory(App\Models\Image::class, 25)->create();
        }
    }
}