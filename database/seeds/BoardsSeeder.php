<?php
use Illuminate\Database\Seeder;

class BoardsSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if(app()->environment()!='production') {
            DB::table('boards')->delete();
            DB::statement("ALTER TABLE boards AUTO_INCREMENT = 1");
            factory(App\Models\Board::class, 5)->create();
        }
    }
}