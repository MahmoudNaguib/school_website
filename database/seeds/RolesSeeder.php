<?php

use Illuminate\Database\Seeder;

class RolesSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if (app()->environment() != 'production') {
            DB::table('roles')->delete();
            DB::statement("ALTER TABLE roles AUTO_INCREMENT = 1");
            insertDefaultRoles();
           // factory(App\Models\Role::class, 5)->create();
        }
    }

}
