<?php
use Illuminate\Database\Seeder;

class GallerySeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if(app()->environment()!='production') {
            DB::table('gallery')->delete();
            DB::statement("ALTER TABLE gallery AUTO_INCREMENT = 1");
            factory(App\Models\Gallery::class, 5)->create();
        }
    }
}