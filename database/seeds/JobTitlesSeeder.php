<?php
use Illuminate\Database\Seeder;

class JobTitlesSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        if(app()->environment()!='production') {
            DB::table('staff')->delete();
            DB::statement("ALTER TABLE staff AUTO_INCREMENT = 1");
            factory(App\Models\JobTitle::class, 5)->create();
        }
    }
}