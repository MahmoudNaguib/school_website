<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration {

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up() {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('profileable_id')->nullable()->index();
            $table->string('profileable_type')->nullable()->index(); //Child, Parent, Staff Models
            $table->bigInteger('role_id')->nullable()->index(); // required if user is admin
            $table->string('name')->nullable(); // required
            $table->string('email')->nullable()->index(); //required|emaill
            $table->string('mobile')->nullable()->index(); //required|mobile
            $table->string('password')->nullable(); //required | min 8 chars
            $table->string('language', 2)->nullable()->default('en')->index();
            $table->string('image', 190)->nullable();
            $table->string('remember_token', 190)->nullable();
            $table->string('confirm_token', 190)->nullable();
            $table->string('last_ip', 190)->nullable();
            $table->timestamp('last_logged_in_at')->nullable();
            $table->boolean('confirmed')->nullable()->default((int) env('USER_CONFIRMATION'));
            $table->boolean('is_active')->nullable()->default(1)->index();
            $table->boolean('is_default')->nullable()->default(0)->index();
            $table->string('google_id')->nullable()->index();
            $table->string('apple_id')->nullable()->index();
            $table->bigInteger('created_by')->nullable()->index();
            $table->timestamp('created_at')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->nullable()->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('deleted_at')->nullable()->index();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down() {
        Schema::dropIfExists('users');
    }

}
