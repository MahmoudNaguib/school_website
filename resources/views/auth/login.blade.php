@extends('layouts.auth')
@section('content')
<div class="signin-box">
    <h3 class="signin-title-secondary">{{$page_title}}</h3>
    {!! Form::open(['method' => 'post'] ) !!}
    {{ csrf_field() }}
    <div class="form-group">
        @php $input = 'email';
        @endphp {!! Form::text($input,request($input),['class'=>'form-control','required'=>'required','placeholder'=>trans('auth.Enter your email')]) !!} @if(@$errors) @foreach($errors->get($input) as $message)
        <span class='help-inline text-danger'>{{ $message }}</span> @endforeach @endif
    </div>
    <!-- form-group -->
    <div class="form-group">
        @php $input = 'password'; @endphp
        {!! Form::password($input,['class'=>'form-control','required'=>'required','placeholder'=>trans('auth.Enter your password')])!!}
        @if(@$errors) 
        @foreach($errors->get($input) as $message)
        <span class='help-inline text-danger'>{{ $message }}</span> 
        @endforeach 
        @endif
    </div>
    <div class="form-group">
        <label class="ckbox">
            <input type="checkbox" name="remember_me"><span>{{trans('auth.Remember me')}}</span>
        </label>
    </div>
    
    
    <!-- form-group -->
    <button class="btn btn-primary btn-block btn-signin">{{ trans('auth.Submit') }}</button>

    <p class="mg-b-0">
        <a href="{{lang()}}/auth/forgot-password">{{ trans('auth.Forgot password') }}</a>
    </p>
    {!! Form::close() !!}
</div>
@endsection
