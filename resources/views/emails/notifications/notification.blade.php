@extends('emails.master')

@section('title'){{trans('front.New Notification From') . " " . appName() }} @endsection

@section('content')
<h2>{{trans("email.New Notification Has Been Received")}}</h2>
<p>
    <label>
        <strong>{{trans("email.Dear")}} </strong>
        {{ \App\Models\User::find($row->to_id)->name }} ,
    </label>
    <br>
<p>
    {{$row->message}}
</p>
<br>
<a href="{{ url($row->url) }}">{{ trans('email.Check it') }}</a>
</p>
@endsection
