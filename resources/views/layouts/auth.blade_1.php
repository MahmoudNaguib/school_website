<!DOCTYPE html>
<html lang="{{ LaravelLocalization::getCurrentLocale() }}" dir="{{ LaravelLocalization::getCurrentLocaleDirection() }}">

    <head>
        @include('admin.partials.meta')
        @include('admin.partials.css')
        @stack('css')
    </head>

    <body>
        <div class="slim-header">
            <div class="container">
                <div class="slim-header-left">
                    @include('admin.partials.logo')
                    @include('admin.partials.search')
                </div>
                <!-- slim-header-left -->
                <div class="slim-header-right">
                    @include('admin.partials.notifications')
                    @include('admin.partials.langSwitch')
                    @include('admin.partials.user_navigation')
                </div>
                <!-- header-right -->
            </div>
            <!-- container -->
        </div>
        <div class="d-md-flex flex-row-reverse">
            @include('admin.partials.flash_messages')
            <div class="signin-right">
                @yield('content')
            </div>
            <!-- signin-right -->
            <div class="signin-left">
                <div class="signin-box">
                    
                    <p>
                        {{ welcomeMessage() }}
                    </p>
                    <p class="tx-12">&copy; {{ trans('auth.Copyright') }} {{ date('Y') }}. {{ trans('auth.All Rights Reserved') }}.</p>
                </div>
            </div>
            <!-- signin-left -->
        </div>
        <!-- d-flex -->
        <!-- signin-wrapper -->
        @include('admin.partials.js') 
        @stack('js')
    </body>

</html>
