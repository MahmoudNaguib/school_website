<div class="row mg-t-20">
    <label class="col-sm-4 form-control-label">{{ @$attributes['label'] }} <span class="tx-danger">{{ (@$attributes['required'])?'*':'' }}</span></label>
    <div class="col-sm-8 mg-t-10 mg-sm-t-0">
        <div class="custom-file">
            {!! Form::File($name,$attributes)!!}
            <label class="custom-file-label" for="{{ @$id }}">{{ trans('app.Choose') }}</label>
            @if(!$errors->isEmpty())
            @foreach($errors->get($name) as $message)
            <span class='help-inline text-danger'>{{ $message }}</span>
            @endforeach
            <br>
            @endif

            @php
            $value=(@$attributes['value'])?:$row->$name;
            $table = $row->getTable();
            @endphp
            @if(@$attributes['file_type'] == 'attachment') 
            <span class="filePreview" id="{{$name}}">
                {!! fileRender($value) !!} 
                @if($value)
                <a class="attachment_delete" href="javascript:;" data-id="{{$row->id}}" data-table="{{$table}}" data-field="{{$name}}" ><i class="fa fa-trash tx-danger"></i></a>
                @endif
            </span>
            @elseif(@$attributes['file_type'] == 'video') 
            <span class="filePreview" id="{{$name}}">
                {!! video($value) !!} 
            </span>
            @else
            <span class="filePreview">
                @if($value)
                {!! image($value,'small') !!}
                @endif
            </span>
            @endif
        </div>
    </div>
</div>
