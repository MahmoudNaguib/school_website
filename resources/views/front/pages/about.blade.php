@extends('front.layouts.master')

@section('content')
<div class="breadcrumb-banner-area" style="background: rgba(0, 0, 0, 0) url(uploads/large/{{$image}}) no-repeat scroll 0 0;">
</div>

<div class="about-page-area section-padding">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title-wrapper">
                    <div class="section-title mt-20">
                        <h3>{{$page_title}}</h3>
                        <p></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-12">
                <div class="about-text-container mb-50">
                    <p>{!! $content!!}</p>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
