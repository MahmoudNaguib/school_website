<div class="col-lg-3 col-md-6 col-12">
    <div class="single-product-item">
        <div class="single-product-image">
            <a href="{{lang()}}/gallery/details/{{$row->id}}">
                <img src="uploads/small/{{$row->image}}" alt="{{$row->title}}">
            </a>
        </div>
        <div class="single-product-text">
            <h4>
                <a href="{{lang()}}/gallery/details/{{$row->id}}">{{$row->title}}</a>
            </h4>
            <h5>({{$row->images->count()}}) {{trans('front.Images')}}</h5>
        </div>
    </div>
</div>