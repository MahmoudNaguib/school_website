@extends('front.layouts.master')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="section-title-wrapper">
            <div class="section-title mt-20">
                <h3>{{$page_title}}</h3>
                <p></p>
            </div>
        </div>
    </div>
</div>
<!--News Details Area Start-->
<div class="news-details-area section-padding">
    <div class="container">
        <div class="row">
            @foreach($row->images as $img)
            @include('front.gallery.single_image')
            @endforeach  
        </div>
    </div>
</div>
<!--End of News Details Area-->
@endsection

