<div class="col-lg-3 col-md-4 mb-30">
    <div class="gallery-img">
        <img src="uploads/small/{{$img->image}}" alt="{{$img->title}}">
        <div class="hover-effect">
            <div class="zoom-icon">
                <a class="popup-image" href="uploads/large/{{$img->image}}"><i class="fa fa-search-plus"></i></a>
            </div>
        </div>
    </div>
</div>