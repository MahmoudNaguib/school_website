<!doctype html>
<html class="no-js" lang="{{ LaravelLocalization::getCurrentLocale() }}" dir="{{ LaravelLocalization::getCurrentLocaleDirection() }}">
    <head>
        @include('front.partials.meta')		
        @include('front.partials.css')
        @stack('css')
    </head>
    <body>
        <!--Main Wrapper Start-->
        <div class="as-mainwrapper">
            <!--Bg White Start-->
            <div class="bg-white">
                <!--Header Area Start-->
                <header class="header-two">
                    @include('front.partials.top_header')
                    @include('front.partials.header')  
                </header>
                <div class="section-padding bg-white">
                    @include('front.partials.flash_messages')
                    @yield('content')
                </div>
                <!--Footer Widget Area Start-->
                @include('front.partials.footer-widget')
                <!--End of Footer Widget Area-->
                <!--Footer Area Start-->
                @include('front.partials.footer')
                <!--End of Footer Area-->
            </div>   
            <!--End of Bg White--> 
        </div>    
        <!--End of Main Wrapper Area--> 
        @include('front.partials.js')
        @stack('js')
    </body>
</html>