<div class="col-lg-4 col-md-6 mt-50">
    <div class="single-item">
        <div class="single-item-image overlay-effect">
            <a href="{{$row->link}}">
                <img src="uploads/small/{{$row->image}}" alt="{{$row->title}}">
            </a>
        </div>
        <div class="single-item-text">
            <h4><a href="{{$row->link}}">
                    {{$row->title_limited}}
                </a></h4>
            <div class="single-item-text-info">
                <span>{{trans('events.Date')}}: <span>{{$row->date}}</span></span>
                <span>{{trans('events.Time')}}: <span>{{$row->time}}</span></span>
            </div>
            <p>{{$row->content_limited}}</p>
        </div>
        <div class="button-bottom">
            <a href="{{$row->link}}" class="button-default">{{trans('front.More')}}</a>
        </div>
    </div>
</div>