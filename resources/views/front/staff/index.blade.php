@extends('front.layouts.master')

@section('content')
<div class="latest-area section-padding bg-white">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title-wrapper">
                    <div class="section-title mt-20">
                        <h3>{{$page_title}}</h3>
                        <p></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            @if (!$rows->isEmpty())
            @foreach ($rows as $row)
            @include('front.staff.single')
            @endforeach
            <div class="container">
                <div class="paganition-center center"> 
                    {!! $rows->appends(['q'=>request('q')])->render() !!}
                </div>
            </div>
            @else
            {{trans("front.There is no results")}}
            @endif
        </div>
    </div>
</div>
@endsection
