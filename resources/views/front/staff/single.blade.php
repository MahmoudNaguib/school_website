<div class="col-lg-3 col-md-6 col-12 mb-20">
    <div class="single-teacher-item">
        <div class="single-teacher-image">
            <a href="{{$row->link}}"><img src="uploads/small/{{$row->image}}" alt="{{$row->name_limited}}"></a>
        </div>
        <div class="single-teacher-text">
            <h3><a href="{{$row->linkt}}">{{$row->name_limited}}</a></h3>
            <h4>{{$row->job_title->title}}</h4>
            <p>{{$row->content_limited}}</p>
        </div>
    </div>
</div>