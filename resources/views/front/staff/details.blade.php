@extends('front.layouts.master')
@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="section-title-wrapper">
            <div class="section-title mt-20">
                <h3>{{trans('front.Staff')}}</h3>
                <p>{{trans('front.Browse our staff')}}</p>
            </div>
        </div>
    </div>
</div>
<!--News Details Area Start-->
<div class="news-details-area section-padding">
    <div class="container">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-12">
                <div class="news-details-content">
                    <div class="single-latest-item">
                        <img src="uploads/large/{{$row->image}}" alt="{{$row->title}}">
                        <div class="single-latest-text">
                            <h3>{{$row->name}}</h3> 
                            <div class="single-item-comment-view">
                                <span><i class="zmdi zmdi-eye"></i>{{$row->views_count}}</span>
                            </div>
                            <p>{!! $row->content !!}</p>
                            <div class="tags-and-links">
                                <div class="social-links">
                                    <span>{{trans('front.Share')}}:</span>
                                    <a href="https://facebook.com/sharer/sharer.php?u={{app()->make("url")->to('/')}}/{{$row->link}}" target="_blank"><i class="zmdi zmdi-facebook"></i></a>
                                    <a href="https://twitter.com/intent/tweet?url={{app()->make("url")->to('/')}}/{{$row->link}}" target="_blank"><i class="zmdi zmdi-twitter"></i></a>
                                    <a href="https://www.linkedin.com/shareArticle?url={{app()->make("url")->to('/')}}/{{$row->link}}" target="_blank"><i class="zmdi zmdi-linkedin"></i></a>
                                    <a href="https://web.whatsapp.com/send?text={{app()->make("url")->to('/')}}/{{$row->link}}" target="_blank"><i class="zmdi zmdi-whatsapp"></i></a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>    
            </div>
            
        </div>
    </div>
</div>
<!--End of News Details Area-->
@endsection

