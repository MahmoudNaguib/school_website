<footer class="footer-area">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-7 col-12">
                <span>
                    {{trans('front.Copyright')}} &copy; {{conf('application_name')}} {{date('Y')}}.
                    <a href="http://www.nagooba.com" target="_blank">{{trans('front.Developed by Mahmoud Naguib')}}</a>
                </span>
            </div>
            <div class="col-lg-6 col-md-5 col-12">
                <div class="column-right">
                    <span>
                        <a href="{{lang()}}/contact" target="_blank">{{trans('front.Contact us')}}</a>
                    </span>
                </div>
            </div>
        </div>
    </div>
</footer>