<li class="{{(request()->is('*/en') || request()->is('*/ar'))?'active':''}}">
    <a href="{{lang()}}">{{trans('front.Home')}}</a>
</li>

<li class="{{request()->is('*/about')?'active':''}}"><a href="{{lang()}}/about">{{trans('front.About')}}</a></li>
<li class="{{request()->is('*/posts')?'active':''}}"><a href="{{lang()}}/posts">{{trans('front.Posts')}}</a></li>
<li class="{{request()->is('*/boards')?'active':''}}"><a href="{{lang()}}/boards">{{trans('front.Honor Board')}}</a></li>

<li class="{{request()->is('*/events')?'active':''}}"><a href="{{lang()}}/events">{{trans('front.Events')}}</a></li>
<li class="{{request()->is('*/staff')?'active':''}}"><a href="{{lang()}}/staff">{{trans('front.Staff')}}</a></li>
<li class="{{request()->is('*/gallery')?'active':''}}"><a href="{{lang()}}/gallery">{{trans('front.Gallery')}}</a></li>
<li class="{{request()->is('*/contact')?'active':''}}"><a href="{{lang()}}/contact">{{trans('front.Contact us')}}</a></li>
