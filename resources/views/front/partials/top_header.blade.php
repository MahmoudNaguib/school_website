<div class="header-top">
    <div class="container">
        <div class="row">
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="pull-left">
                    <span>{{trans('front.Phone')}}: {{conf('phone')}}</span>&nbsp;&nbsp;&nbsp;
                    <span>{{trans('front.Email')}}: {{conf('email')}}</span>
                    <span>
                        @if(lang()=='ar')
                        <a href="{{urlLang(url()->full(),lang(),'en')}}" class="nav-link langSwitch">English</a>
                        @else
                        <a href="{{urlLang(url()->full(),lang(),'ar')}}" class="nav-link langSwitch">عربي</a>
                        @endif
                    </span>
                </div>
            </div>
            <div class="col-lg-6 col-md-6 col-sm-12">
                <div class="header-top-right pull-right">
                    <div class="content">
                        @if(auth()->user())
                        <i class="zmdi zmdi-account"></i> {{trans('front.Welecome')}} {{str_limit(auth()->user()->name,10)}}
                        <ul class="account-dropdown">
                            @if(auth()->user()->is_admin)
                            <li><a href="{{lang()}}/admin">{{ trans('front.Admin Dashboard') }}</a></li>
                            @endif
                            <li><a href="{{lang()}}/profile/edit">{{trans('front.Edit account')}}</a></li>
                            <li><a href="{{lang()}}/profile/change-password">{{trans('front.Change password')}}</a></li>
                            <li><a href="{{lang()}}/profile/logout">{{trans('front.Logout')}}</a></li>
                        </ul>
                        @else
                        <a href="{{lang()}}/auth/login">{{trans('front.Login')}}</a>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>