<div class="header-logo-menu sticker">
    <div class="container">
        <div class="row">
            <div class="col-lg-4 col-12">
                <div class="logo">
                    <a href="{{lang()}}">
                        <img src="uploads/small/{{conf('logo')}}" alt="{{conf('application_name')}}">
                        {{conf('application_name')}}
                    </a>
                </div>
            </div>
            <div class="col-lg-8 col-12">
                <div class="mainmenu-area pull-right">
                    <div class="mainmenu d-none d-lg-block">
                        <nav>
                            <ul id="nav">
                                @include('front.partials.nav') 
                            </ul>
                        </nav>
                    </div>
                </div> 
            </div>
        </div>
    </div>
</div>  
<!-- Mobile Menu Area start -->
<div class="mobile-menu-area">
    <div class="container clearfix">
        <div class="row">
            <div class="col-lg-12 col-md-12 col-sm-12">
                <div class="mobile-menu">
                    <nav id="dropdown">
                        <ul>
                            @include('front.partials.nav') 
                        </ul>
                    </nav>
                </div>					
            </div>
        </div>
    </div>
</div>
<!-- Mobile Menu Area end --> 