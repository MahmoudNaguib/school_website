<div class="col-lg-6 col-md-12 col-12">
    <div class="single-latest-item">
        <div class="single-latest-image">
            <a href="{{$row->link}}">
                <img src="uploads/small/{{$row->image}}" alt="{{$row->title}}">
            </a>
        </div>
        <div class="single-latest-text">
            <h3>
                <a href="{{$row->link}}">
                    {{$row->title_limited}}
                </a>
            </h3>
            <div class="single-item-comment-view">
                <span><i class="zmdi zmdi-calendar-check"></i>{{$row->created_at->format('F j, Y')}}</span>
                <span><i class="zmdi zmdi-eye"></i>{{$row->views_count}}</span>
            </div>
            <p>
                {{$row->content_limited}}
            </p>
            <a href="{{$row->link}}" class="button-default">{{trans('front.More')}}</a>
        </div>
    </div>
</div>