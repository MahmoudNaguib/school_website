<div class="col-lg-3 col-md-6 col-12 mb-20">
    <div class="single-teacher-item">
        <div class="single-teacher-image">
            <a href="{{$row->link}}"><img src="uploads/small/{{$row->image}}" alt="{{$row->title}}"></a>
        </div>
        <div class="single-teacher-text">
            <h3>{{$row->title}}</h3>
            <p>{{$row->content}}</p>
        </div>
    </div>
</div>