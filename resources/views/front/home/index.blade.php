@extends('front.layouts.master')

@section('content')
<!--End of Header Area-->
<!--Background Area Start-->
<div class="background-area no-animation">	
    <img src="uploads/large/{{conf('home_banner')}}" alt="Home Banner"/>
    <div class="banner-content static-text">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="text-content-wrapper full-width">
                        <div class="text-content text-center">
                            <h1 class="title1 text-center mb-20">
                                {{conf('application_name')}}<br>
                               {{conf('home_banner_text')}}
                            </h1>
                        </div>
                    </div>
                </div>
            </div>
        </div>	 
    </div>
</div>

@if (!$latestImages->isEmpty())
<div class="latest-area section-padding bg-white mt-30">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title-wrapper">
                    <div class="section-title">
                        <h3>{{trans('front.Latest images')}}</h3>
                        <p></p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            @foreach($latestImages as $img)
            @include('front.gallery.single_image')
            @endforeach
            <div class="col-md-12 col-sm-12 text-center">
                <a href="{{lang()}}/gallery" class="button-default button-large">{{trans('front.Browse our gallery')}} <i class="zmdi zmdi-chevron-right"></i></a>
            </div>
        </div>
    </div>
</div>
@endif

<div class="vision mt-30 pt-40 pb-40" style="background: rgba(0, 0, 0, 0) url(uploads/large/{{conf('vision_banner')}}) no-repeat scroll center top;">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12">
                <h3>{{trans('front.Our vision')}}</h3>
                <p>
                    {{conf('vision_text')}}
                </p>
            </div>
        </div>
    </div>
</div>


@if (!$boards->isEmpty())
<!--Teachers Area Start-->
<div class="teachers-area mt-30">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title-wrapper">
                    <div class="section-title">
                        <h3>{{trans('front.Honor Board')}}</h3>
                        <p></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach($boards as $row)
            @include('front.boards.single')
            @endforeach
            <div class="col-md-12 col-sm-12 text-center">
                <a href="{{lang()}}/boards" class="button-default button-large">{{trans('front.Honor Board')}} <i class="zmdi zmdi-chevron-right"></i></a>
            </div>
        </div>
    </div>
</div>
<!--End of Teachers Area-->
@endif

<div class="vision mt-30 pt-40 pb-40" style="background: rgba(0, 0, 0, 0) url(uploads/large/{{conf('mission_banner')}}) no-repeat scroll center top;">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 col-md-12">
                <h3>{{trans('front.Our mission')}}?</h3>
                    <p>
                        {{conf('mission_text')}}
                    </p>
            </div>
        </div>
    </div>
</div>


@if (!$latestPosts->isEmpty())
<div class="latest-area section-padding bg-white mt-30">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title-wrapper">
                    <div class="section-title">
                        <h3>{{trans('front.Latest posts')}}</h3>
                        <p></p>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            @foreach($latestPosts as $row)
            @include('front.posts.single')
            @endforeach
            <div class="col-md-12 col-sm-12 text-center">
                <a href="{{lang()}}/posts" class="button-default button-large">{{trans('front.Browse All Posts')}} <i class="zmdi zmdi-chevron-right"></i></a>
            </div>
        </div>
    </div>
</div>
@endif
<!--End of Latest News Area--> 

@if(!$latestEvents->isEmpty())
<!--Events Area Start-->
<div class="course-area section-padding mt-30">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title-wrapper">
                    <div class="section-title">
                        <h3>{{trans('front.Latest events')}}</h3>
                        <p></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach($latestEvents as $row)
            @include('front.events.single')
            @endforeach
            <div class="col-md-12 col-sm-12 text-center">
                <a href="{{lang()}}/events" class="button-default button-large">{{trans('front.Browse All Events')}} <i class="zmdi zmdi-chevron-right"></i></a>
            </div>
        </div>
    </div>
</div>
<!--End of Event Area-->
@endif


@if (!$staff->isEmpty())
<!--Teachers Area Start-->
<div class="teachers-area mt-30">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="section-title-wrapper">
                    <div class="section-title">
                        <h3>{{trans('front.OUR Staff')}}</h3>
                        <p></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            @foreach($staff as $row)
            @include('front.staff.single')
            @endforeach
            <div class="col-md-12 col-sm-12 text-center">
                <a href="{{lang()}}/staff" class="button-default button-large">{{trans('front.Browse All Staff')}} <i class="zmdi zmdi-chevron-right"></i></a>
            </div>
        </div>
    </div>
</div>
<!--End of Teachers Area-->
@endif


@endsection
