@foreach(langs() as $lang)
@php
    $attributes=['class'=>'form-control','label'=>trans('boards.Title').' '.$lang,'placeholder'=>trans('boards.Title')];
    if($lang=='en')
        $attributes['required']=1;
@endphp
@include('form.input',['name'=>'title['.$lang.']','value'=>$row->getTranslation('title',$lang),'type'=>'text','attributes'=>$attributes])
@endforeach


@foreach(langs() as $lang)
@php
    $attributes=['class'=>'form-control','label'=>trans('boards.Content').' '.$lang,'placeholder'=>trans('boards.Content')];
    if($lang=='en')
        $attributes['required']=1;
@endphp
@include('form.input',['name'=>'content['.$lang.']','value'=>$row->getTranslation('content',$lang),'type'=>'text','attributes'=>$attributes])
@endforeach



@include('form.boolean',['name'=>'is_active','attributes'=>['label'=>trans('boards.Is active')]])


@include('form.file',['name'=>'image','attributes'=>['class'=>'form-control custom-file-input','label'=>trans('boards.Image'),'placeholder'=>trans('boards.Image')]])


@push('js')
<script>
    $(function () {
	$('form').submit(function () {
            var title_en=$('input[name="title[en]"]').val();
            @foreach(langs() as $lang)
                if($('input[name="title[{{$lang}}]"]').val()=='')
                    $('input[name="title[{{$lang}}]"]').val(title_en);
            @endforeach
        });
});
</script>
@endpush
