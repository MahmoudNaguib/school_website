<div class="mg-b-10">
    {!! Form::model($row,['method' => 'get','files' => true] ) !!} 
    <div class="row">

        <div class="col-lg-3 col-md-6 mg-t-10">
            {!! Form::select('imageable_id',  $row->getGalleries(),@request('imageable_id'), ['class'=>'form-control select2','placeholder'=>trans('images.Gallery')]) !!}
            <input type="hidden" name="imageable_type" value="App\Models\Gallery">
        </div><!-- col-4 -->

        <div class="col-lg-3 col-md-6 mg-t-10">
            <button class="btn btn-primary col-lg-5 col-md-5 mg-b-10">{{ trans('app.Filter') }}</button> 
            <a href="{{$module}}" class="btn btn-primary col-lg-5 col-md-5 mg-b-10">{{ trans('app.Reset') }}</a>
        </div>
    </div><!-- row -->
    {!! Form::close() !!}
</div><!-- section-wrapper -->