@include('form.select',['name'=>'imageable_id','options'=>$row->getGalleries(),'attributes'=>['class'=>'form-control select2','label'=>trans('images.Gallery'),'placeholder'=>trans('images.Select Gallery'),'required'=>1]])
<input type="hidden" name="imageable_type" value="App\Models\Gallery">

@foreach(langs() as $lang)
@php
    $attributes=['class'=>'form-control','label'=>trans('images.Title').' '.$lang,'placeholder'=>trans('images.Title')];
    if($lang=='en')
        $attributes['required']=1;
@endphp
@include('form.input',['name'=>'title['.$lang.']','value'=>$row->getTranslation('title',$lang),'type'=>'text','attributes'=>$attributes])
@endforeach

@include('form.file',['name'=>'image','attributes'=>['class'=>'form-control custom-file-input','label'=>trans('images.Image'),'placeholder'=>trans('images.Image')]])

@include('form.boolean',['name'=>'is_active','attributes'=>['label'=>trans('images.Is active')]])

@push('js')
<script>
    $(function () {
	$('form').submit(function () {
            var title_en=$('input[name="title[en]"]').val();
            @foreach(langs() as $lang)
                if($('input[name="title[{{$lang}}]"]').val()=='')
                    $('input[name="title[{{$lang}}]"]').val(title_en);
            @endforeach
        });
});
</script>
@endpush
