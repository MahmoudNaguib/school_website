@extends('admin.layouts.master')
@section('title')
<h6 class="slim-pagetitle">
    {{ @$page_title }}
</h6>
@endsection
@section('content')
<div class="section-wrapper">
    @if(request('q')) 
        @if(!@$posts->isEmpty())
        <h4>{{trans('search.Posts')}}</h4>
        <ul>
            @foreach($posts as $row)
            <li><a href="posts/view/{{$row->id}}" target="_blank">{{$row->title}}</a></li>
            @endforeach
        </ul>
        @endif
        @if(!@$events->isEmpty())
        <h4>{{trans('search.Events')}}</h4>
        <ul>
            @foreach($events as $row)
            <li><a href="events/view/{{$row->id}}" target="_blank">{{$row->title}}</a></li>
            @endforeach
        </ul>
        @endif
        @if(!@$gallery->isEmpty())
        <h4>{{trans('search.Gallery')}}</h4>
        <ul>
            @foreach($gallery as $row)
            <li><a href="gallery/view/{{$row->id}}" target="_blank">{{$row->title}}</a></li>
            @endforeach
        </ul>
        @endif
        @if(!@$images->isEmpty())
        <h4>{{trans('search.Images')}}</h4>
        <ul>
            @foreach($images as $row)
            <li><a href="images/view/{{$row->id}}" target="_blank">{{$row->title}}</a></li>
            @endforeach
        </ul>
        @endif
        
    @endif
</div>
@endsection
