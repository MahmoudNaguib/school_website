@foreach(langs() as $lang)
@php
    $attributes=['class'=>'form-control','label'=>trans('gallery.Title').' '.$lang,'placeholder'=>trans('gallery.Title')];
    if($lang=='en')
        $attributes['required']=1;
@endphp
@include('form.input',['name'=>'title['.$lang.']','value'=>$row->getTranslation('title',$lang),'type'=>'text','attributes'=>$attributes])
@endforeach

@include('form.boolean',['name'=>'is_active','attributes'=>['label'=>trans('gallery.Is active')]])


@include('form.file',['name'=>'image','attributes'=>['class'=>'form-control custom-file-input','label'=>trans('gallery.Image'),'placeholder'=>trans('gallery.Image')]])


@push('js')
<script>
    $(function () {
	$('form').submit(function () {
            var title_en=$('input[name="title[en]"]').val();
            @foreach(langs() as $lang)
                if($('input[name="title[{{$lang}}]"]').val()=='')
                    $('input[name="title[{{$lang}}]"]').val(title_en);
            @endforeach
        });
});
</script>
@endpush
