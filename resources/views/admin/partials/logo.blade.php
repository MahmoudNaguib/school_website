<h2 class="slim-logo">
    <a class="navbar-brand" href="{{app()->make("url")->to('/').'/'.lang()}}">
        <img src="uploads/small/{{conf('logo')}}" alt="logo">
    </a>
</h2>