<div class="slim-navbar">
    <div class="container">
        <ul class="nav">
            <li class="nav-item {{(request()->is(lang()))?"active":""}}">
                <a class="nav-link" href="{{app()->make("url")->to('/')}}/{{lang()}}/admin">
                    <i class="icon ion-ios-pie-outline"></i>
                    <span>{{trans('navigation.Dashboard')}}</span>
                </a>
            </li>

            @if(can('create-users') || can('view-users'))
            <li class="nav-item {{(request()->is('*/users*'))?"active":""}}">
                <a class="nav-link" href="{{lang()}}/admin/users">
                    <i class="icon fa ion-ios-contact-outline"></i>
                    <span>{{trans('navigation.Users')}}</span>
                </a>
            </li>
            @endif
            
            @if(can('create-posts') || can('view-posts'))
            <li class="nav-item {{(request()->is('*/posts*'))?"active":""}}">
                <a class="nav-link" href="{{lang()}}/admin/posts">
                    <i class="icon fa ion-ios-compose"></i>
                    <span>{{trans('navigation.Posts')}}</span>
                </a>
            </li>
            @endif
            
            @if(can('create-events') || can('view-events'))
            <li class="nav-item {{(request()->is('*/events*'))?"active":""}}">
                <a class="nav-link" href="{{lang()}}/admin/events">
                    <i class="icon fa ion-ios-compose"></i>
                    <span>{{trans('navigation.Events')}}</span>
                </a>
            </li>
            @endif
            
            @if(can('create-boards') || can('view-boards'))
            <li class="nav-item {{(request()->is('*/boards*'))?"active":""}}">
                <a class="nav-link" href="{{lang()}}/admin/boards">
                    <i class="icon fa ion-ios-compose"></i>
                    <span>{{trans('navigation.Honor Boards')}}</span>
                </a>
            </li>
            @endif

            @if(can('create-staff') || can('view-staff') || can('create-job_titles') || can('view-job_titles'))
            <li class="nav-item with-sub settings {{(request()->is('*/staff*') || request()->is('*/job_titles*'))?"active":""}}">
                <a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="true">
                    <i class="icon fa ion-ios-contact"></i>
                    <span>{{trans('navigation.Staff')}}</span>
                </a>
                <div class="sub-item">
                    <ul>
                        @if(can('create-job_titles') || can('view-job_titles'))
                        <li class="{{(request()->is('*/job_titles*'))?"active":""}}">
                            <a href="{{lang()}}/admin/job_titles">{{trans('navigation.Job titles')}}</a>
                        </li>
                        @endif
                        @if(can('create-staff') || can('view-staff'))
                        <li class="{{(request()->is('*/staff*'))?"active":""}}">
                            <a href="{{lang()}}/admin/staff">{{trans('navigation.Staff')}}</a>
                        </li>
                        @endif
                    </ul>
                </div><!-- dropdown-menu -->
            </li>
            @endif
            
            
            
            @if(can('create-gallery') || can('view-gallery') || can('create-gallery') || can('view-gallery'))
            <li class="nav-item with-sub settings {{(request()->is('*/gallery*') || request()->is('*/images*'))?"active":""}}">
                <a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="true">
                    <i class="icon fa ion-ios-compose"></i>
                    <span>{{trans('navigation.Gallery')}}</span>
                </a>
                <div class="sub-item">
                    <ul>
                        @if(can('create-gallery') || can('view-gallery'))
                        <li class="{{(request()->is('*/gallery*'))?"active":""}}">
                            <a href="{{lang()}}/admin/gallery">{{trans('navigation.Gallery')}}</a>
                        </li>
                        @endif
                        @if(can('create-images') || can('view-images'))
                        <li class="{{(request()->is('*/images*'))?"active":""}}">
                            <a href="{{lang()}}/admin/images">{{trans('navigation.Images')}}</a>
                        </li>
                        @endif
                    </ul>
                </div><!-- dropdown-menu -->
            </li>
            @endif
            
            @if(can('view-messages'))
            <li class="nav-item {{(request()->is('*/messages*'))?"active":""}}">
                <a class="nav-link" href="{{lang()}}/admin/messages">
                    <i class="icon fa ion-ios-compose"></i>
                    <span>{{trans('navigation.Messages')}}</span>
                </a>
            </li>
            @endif
            
            @if(@auth()->user()->is_super_admin)
            <li class="nav-item with-sub settings">
                <a class="nav-link" href="#" data-toggle="dropdown" aria-expanded="true">
                    <i class="icon ion-ios-gear-outline"></i>
                    <span>{{trans('navigation.Settings')}}</span>
                </a>
                <div class="sub-item">
                    <ul>
                        <li class="{{(request()->is('*/configs*'))?"active":""}}">
                            <a href="{{lang()}}/admin/configs">{{trans('navigation.Configurations')}}</a>
                        </li>
                        <li class="{{(request()->is('*/roles*'))?"active":""}}">
                            <a href="{{lang()}}/admin/roles">{{trans('navigation.Roles')}}</a>
                        </li>
                    </ul>
                </div><!-- dropdown-menu -->
            </li>
            @endif

        </ul>
    </div>
    <!-- container -->
</div>