@include('form.select',['name'=>'job_title_id','options'=>$row->getJobTitles(),'attributes'=>['class'=>'form-control select2','label'=>trans('staff.Gallery'),'placeholder'=>trans('staff.Select Job title'),'required'=>1]])
@foreach(langs() as $lang)
@php
    $attributes=['class'=>'form-control','label'=>trans('staff.Name').' '.$lang,'placeholder'=>trans('staff.Name')];
    if($lang=='en')
        $attributes['required']=1;
@endphp
@include('form.input',['name'=>'name['.$lang.']','value'=>$row->getTranslation('name',$lang),'type'=>'text','attributes'=>$attributes])
@endforeach


@foreach(langs() as $lang)
@php
    $attributes=['class'=>'form-control editor','label'=>trans('staff.Content').' '.$lang,'placeholder'=>trans('staff.Content')];
    if($lang=='en')
        $attributes['required']=1;
@endphp
@include('form.input',['name'=>'content['.$lang.']','value'=>$row->getTranslation('content',$lang),'type'=>'textarea','attributes'=>$attributes])
@endforeach

@foreach(langs() as $lang)
@php
    $attributes=['class'=>'form-control','label'=>trans('staff.Meta description').' '.$lang,'placeholder'=>trans('staff.Meta description')];
@endphp

@include('form.input',['name'=>'meta_description['.$lang.']','value'=>$row->getTranslation('meta_description',$lang),'type'=>'textarea','attributes'=>$attributes])
@endforeach

@foreach(langs() as $lang)
@php
    $attributes=['class'=>'form-control tags','label'=>trans('staff.Meta keywords').' '.$lang,'placeholder'=>trans('staff.Meta keywords')];
@endphp

@include('form.input',['name'=>'meta_keywords['.$lang.']','value'=>$row->getTranslation('meta_keywords',$lang),'type'=>'text','attributes'=>$attributes])
@endforeach

@include('form.file',['name'=>'image','attributes'=>['class'=>'form-control custom-file-input','label'=>trans('staff.Image'),'placeholder'=>trans('staff.Image')]])

@include('form.boolean',['name'=>'is_active','attributes'=>['label'=>trans('staff.Is active')]])
@include('form.boolean',['name'=>'show_in_home_page','attributes'=>['label'=>trans('staff.Show in home page')]])

@push('js')
<script>
    $(function () {
	$('form').submit(function () {
            var name_en=$('input[name="name[en]"]').val();
            @foreach(langs() as $lang)
                if($('input[name="name[{{$lang}}]"]').val()=='')
                    $('input[name="name[{{$lang}}]"]').val(name_en);
            @endforeach
            
            var content_en=$('textarea[name="content[en]"]').val();
            @foreach(langs() as $lang)
                if($('textarea[name="content[{{$lang}}]"]').val()=='')
                    $('textarea[name="content[{{$lang}}]"]').val(content_en);
            @endforeach
            
            var meta_description_en=$('textarea[name="meta_description[en]"]').val();
            @foreach(langs() as $lang)
                if($('textarea[name="meta_description[{{$lang}}]"]').val()=='')
                    $('textarea[name="meta_description[{{$lang}}]"]').val(meta_description_en);
            @endforeach
            
            var meta_keywords_en=$('input[name="meta_keywords[en]"]').val();
            @foreach(langs() as $lang)
                if($('input[name="meta_keywords[{{$lang}}]"]').val()=='')
                    $('input[name="meta_keywords[{{$lang}}]"]').val(meta_keywords_en);
            @endforeach

            
        });
});
</script>
@endpush
