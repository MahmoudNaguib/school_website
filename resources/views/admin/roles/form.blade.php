@php
$attributes=['class'=>'form-control','label'=>trans('roles.Title'),'placeholder'=>trans('roles.Title'),'required'=>1];
@endphp
@include('form.input',['name'=>'title','type'=>'text','attributes'=>$attributes])

@if(config('modules'))
@foreach(config('modules') as $key=>$permissions)
@php $parentId=str_replace('\\','_',$key) @endphp
<h5 class="mg-b-10 mg-t-20">
    <label class="ckbox">
        {!! Form::checkbox('parents',NULL,null,['id'=>$parentId,'class'=>'parents']) !!}
        <span><b><u>{{ucfirst($key)}}</u></b></span>
    </label>
</h5>
<div class="row">
    @foreach ($permissions as $permission)
    <div class="col-lg-3">
        <label class="ckbox">
            {!! Form::checkbox('permissions[]',$permission.'-'.$key,null,['id'=>$permission.'-'.$key,'class'=>'childs childs_'.$parentId,'for'=>$parentId]) !!}
            <span>{{ucfirst($permission)}} {{ucfirst($key)}}</span>
        </label>
    </div>
    @endforeach
</div>
@endforeach
@endif
@if(@$errors)
@foreach($errors->get('permissions') as $message)
<span class='help-inline text-danger'>{{trans('roles.Choose at least 1 permission')}}</span>
@endforeach
@endif

@push('js')
<script>
    $('.parents').on('change',function(){
        if ($(this).is(':checked')) {
            $('.childs_'+$(this).attr('id')).prop('checked', true);
        }
        else{
            $('.childs_'+$(this).attr('id')).prop('checked', false);
        }
    });
    $('.childs').on('change',function(){
        var parent=$(this).attr("for");
        if ($(this).is(':checked')) {
            $('#'+parent).prop('checked', true);
        }
        else{
            if($('.childs_'+parent+":checked").size() ==0){
                $('#'+parent).prop('checked', false);
            }
        }
    });
    $('.childs').trigger('change');
    
</script>
@endpush