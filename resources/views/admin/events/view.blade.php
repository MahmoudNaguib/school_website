@extends('admin.layouts.master')
@section('title')
<h6 class="slim-pagetitle">
    {{ @$page_title }}
</h6>
@endsection
@section('content')
<div class="section-wrapper">
    @if(can('edit-'.$module))
    <a href="{{$module}}/edit/{{$row->id}}" class="btn btn-success">
        <i class="fa fa-edit"></i> {{trans('events.Edit')}}
    </a><br>
    @endif
    <div class="table-responsive">
        <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered pull-left">
            @foreach(langs() as $lang)
            <tr>
                <td width="25%" class="align-left">{{trans('events.Title')}} {{$lang}}</td>
                <td width="75%" class="align-left">{{@$row->getTranslation('title',$lang)}}</td>
            </tr>
            @endforeach

            @foreach(langs() as $lang)
            <tr>
                <td width="25%" class="align-left">{{trans('events.Slug')}} {{$lang}}</td>
                <td width="75%" class="align-left">{{@$row->getTranslation('slug',$lang)}}</td>
            </tr>
            @endforeach

            @foreach(langs() as $lang)
            <tr>
                <td width="25%" class="align-left">{{trans('events.Content')}} {{$lang}}</td>
                <td width="75%" class="align-left">{!! @$row->getTranslation('content',$lang) !!}</td>
            </tr>
            @endforeach
            
            <tr>
                <td width="25%" class="align-left">{{trans('events.Date')}}</td>
                <td width="75%" class="align-left">{{ @$row->date }}</td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('events.Time')}}</td>
                <td width="75%" class="align-left">{{ @$row->time }}</td>
            </tr>
            
            @foreach(langs() as $lang)
            <tr>
                <td width="25%" class="align-left">{{trans('events.Tags')}} {{$lang}}</td>
                <td width="75%" class="align-left">{{@$row->getTranslation('tags',$lang)}}</td>
            </tr>
            @endforeach

            @foreach(langs() as $lang)
            <tr>
                <td width="25%" class="align-left">{{trans('events.Meta description')}} {{$lang}}</td>
                <td width="75%" class="align-left">{{@$row->getTranslation('meta_description',$lang)}}</td>
            </tr>
            @endforeach

            @foreach(langs() as $lang)
            <tr>
                <td width="25%" class="align-left">{{trans('events.Meta keywords')}} {{$lang}}</td>
                <td width="75%" class="align-left">{{@$row->getTranslation('meta_keywords',$lang)}}</td>
            </tr>
            @endforeach

            <tr>
                <td width="25%" class="align-left">{{trans('events.Image')}}</td>
                <td width="75%" class="align-left">{!! image($row->image,'small') !!}</td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('events.Is active')}}</td>
                <td width="75%" class="align-left"><img src="backend/img/{{($row->is_active)?'check.png':'close.png'}}"></td>
            </tr>
            <tr>
                <td width="25%" class="align-left">{{trans('events.Created by')}}</td>
                <td width="75%" class="align-left">{{@$row->creator->name}}</td>
            </tr>

        </table>
    </div>
</div>
@endsection
