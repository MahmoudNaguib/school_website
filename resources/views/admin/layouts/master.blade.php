<!DOCTYPE html>
<html lang="{{ LaravelLocalization::getCurrentLocale() }}" dir="ltr">
    <head>
        @include('admin.partials.meta')
        @include('admin.partials.css') @stack('css')
    </head>
    
    <body>
        <div class="slim-header">
            <div class="container">
                <div class="slim-header-left">
                    @include('admin.partials.logo')
                    @include('admin.partials.search')
                    <!-- search-box -->
                </div>
                <!-- slim-header-left -->
                <div class="slim-header-right">
                    @include('admin.partials.notifications')
                    @include('admin.partials.langSwitch')
                    @include('admin.partials.user_navigation')
                </div>
                <!-- header-right -->
            </div>
            <!-- container -->
        </div>
        <!-- slim-header -->
        @include('admin.partials.navigation')
        <!-- slim-navbar -->
        <div class="slim-mainpanel">
            <div class="container">
                @include('admin.partials.breadcrumb')
                @include('admin.partials.flash_messages')
                <!-- section-wrapper -->
                @yield('content')
                <!-- section-wrapper -->
            </div>
            <!-- container -->
        </div>
        <!-- slim-mainpanel -->
        <!-- slim-footer -->
        @include('admin.partials.footer')
        @include('admin.partials.js') @stack('js')
    </body>

</html>
