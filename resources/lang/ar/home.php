<?php 

return [
    '0' => '1',
    'View Courses' => 'View Courses',
    'Why us' => 'Why us',
    'POPULAR COURSES' => 'POPULAR COURSES',
    'IMPORTANT FACTS' => 'IMPORTANT FACTS',
    'Latest News' => 'Latest News',
    'Our vision' => 'Our vision',
    'Our mission' => 'Our mission',
    'Latest events' => 'Latest events',
    'Be notified with latest events' => 'Be notified with latest events',
    'More' => 'More',
    'Browse All Events' => 'Browse All Events',
    'Latest posts' => 'Latest posts',
    'Be notified with latest posts' => 'Be notified with latest posts',
    'OUR Staff' => 'OUR Staff',
    'They do their best' => 'They do their best',
];