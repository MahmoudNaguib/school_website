<?php 

return [
    '0' => '1',
    'Name' => 'Name',
    'Email' => 'Email',
    'Mobile' => 'Mobile',
    'Default language' => 'Default language',
    'Password' => 'Password',
    'Password confirmation' => 'Password confirmation',
    'Image' => 'Image',
    'Save' => 'Save',
    'Edit Profile' => 'Edit Profile',
    'Bio' => 'Bio',
    'Meta description' => 'Meta description',
    'Meta keywords' => 'Meta keywords',
    'Change password' => 'Change password',
];