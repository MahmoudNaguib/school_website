<?php 

return [
    '0' => '1',
    'Title' => 'Title',
    'Is active' => 'Is active',
    'ID' => 'ID',
    'Active' => 'Active',
    'Created at' => 'Created at',
    'Edit' => 'Edit',
    'View' => 'View',
    'Delete' => 'Delete',
    'Are you sure you want to delete this item' => 'Are you sure you want to delete this item',
    'There is no results' => 'There is no results',
    'Created by' => 'Created by',
    'Image' => 'Image',
    'Views count' => 'Views count',
];