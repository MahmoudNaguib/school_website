<?php 

return [
    '0' => '1',
    'Gallery' => 'Gallery',
    'Select Gallery' => 'Select Gallery',
    'Title' => 'Title',
    'Image' => 'Image',
    'Is active' => 'Is active',
    'Edit' => 'Edit',
    'Created by' => 'Created by',
    'ID' => 'ID',
    'Active' => 'Active',
    'Created at' => 'Created at',
    'View' => 'View',
    'Delete' => 'Delete',
    'Are you sure you want to delete this item' => 'Are you sure you want to delete this item',
    'There is no results' => 'There is no results',
];