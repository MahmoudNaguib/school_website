<?php 

return [
    '0' => '1',
    'Forgot your password' => 'Forgot your password',
    'Welcome' => 'Welcome',
    'Thanks for joining us at' => 'Thanks for joining us at',
    'Here is your account details' => 'Here is your account details',
    'Name' => 'Name',
    'Email' => 'Email',
    'New Password' => 'New Password',
    'Registertion' => 'Registertion',
    'Mobile' => 'Mobile',
    'To activate your account please click the link below' => 'To activate your account please click the link below',
    'Team' => 'Team',
    'Copyright' => 'Copyright',
    'New Notification Has Been Received' => 'New Notification Has Been Received',
    'Dear' => 'Dear',
    'Check it' => 'Check it',
    'Reminder Has Been Received' => 'Reminder Has Been Received',
    'Reset password' => 'Reset password',
    'Notification Mail' => 'Notification Mail',
    'Welcome to' => 'Welcome to',
    'Contact us submit form' => 'Contact us submit form',
    'Content' => 'Content',
];