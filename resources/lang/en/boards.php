<?php 

return [
    '0' => '1',
    'Title' => 'Title',
    'Content' => 'Content',
    'Is active' => 'Is active',
    'Image' => 'Image',
    'Edit' => 'Edit',
    'Created by' => 'Created by',
    'ID' => 'ID',
    'Views count' => 'Views count',
    'Active' => 'Active',
    'Created at' => 'Created at',
    'View' => 'View',
    'Delete' => 'Delete',
    'Are you sure you want to delete this item' => 'Are you sure you want to delete this item',
    'There is no results' => 'There is no results',
];