<?php 

return [
    '0' => '1',
    'ID' => 'ID',
    'Name' => 'Name',
    'Email' => 'Email',
    'Content' => 'Content',
    'Created at' => 'Created at',
    'View' => 'View',
    'Delete' => 'Delete',
    'Are you sure you want to delete this item' => 'Are you sure you want to delete this item',
    'There is no results' => 'There is no results',
    'Mobile' => 'Mobile',
];