<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class RolesControllerTest extends TestCase {

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_list_roles() {
        dump('test_list_roles');
        $user = \App\Models\User::find(2);
        $latest = \App\Models\Role::orderBy('id', 'desc')->first();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/roles')
                ->assertStatus(200)
                ->assertSee('roles');
    }

    public function test_create_roles() {
        dump('test_create_roles');
        $user = \App\Models\User::find(2);
        $row = factory(\App\Models\Role::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->post('en/roles/create', $row->toArray());
        $latest = \App\Models\Role::orderBy('id', 'desc')->first();
        $this->assertEquals($row->title, $latest->title);
        $latest->forceDelete();
    }

    public function test_edit_roles() {
        dump('test_edit_roles');
        $user = \App\Models\User::find(2);
        $record = \App\Models\Role::create(factory(\App\Models\Role::class)->make()->toArray());
        $row = factory(\App\Models\Role::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->post('en/roles/edit/' . $record->id, $row->toArray());
        $record = \App\Models\Role::find($record->id);
        $this->assertEquals($record->title, $row->title);
        $record->forceDelete();
    }

    public function test_delete_roles() {
        dump('test_delete_roles');
        $user = \App\Models\User::find(2);
        $record = \App\Models\Role::create(factory(\App\Models\Role::class)->make()->toArray());
        $row = factory(\App\Models\Role::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/roles/delete/' . $record->id);
        $this->assertEquals('a', 'a');
        $record->forceDelete();
    }

}
