<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class StaffControllerTest extends TestCase {

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_list_staff() {
        dump('test_list_staff');
        $user = \App\Models\User::find(2);
        $latest = \App\Models\Staff::orderBy('id', 'desc')->first();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/staff')
                ->assertStatus(200)
                ->assertSee('staff');
    }

    public function test_create_staff() {
        dump('test_create_staff');
        $user = \App\Models\User::find(2);
        $row = factory(\App\Models\Staff::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->post('en/staff/create', $row->toArray());
        $latest = \App\Models\Staff::orderBy('id', 'desc')->first();
        $this->assertEquals($row->title, $latest->title);
        $latest->forceDelete();
    }

    public function test_edit_staff() {
        dump('test_edit_staff');
        $user = \App\Models\User::find(2);
        $record = \App\Models\Staff::create(factory(\App\Models\Staff::class)->make()->toArray());
        $row = factory(\App\Models\Staff::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->post('en/staff/edit/' . $record->id, $row->toArray());
        $record = \App\Models\Staff::find($record->id);
        $this->assertEquals($record->title, $row->title);
        $record->forceDelete();
    }

    public function test_delete_staff() {
        dump('test_delete_staff');
        $user = \App\Models\User::find(2);
        $record = \App\Models\Staff::create(factory(\App\Models\Staff::class)->make()->toArray());
        $row = factory(\App\Models\Staff::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/staff/delete/' . $record->id);
        $this->assertEquals('a', 'a');
        $record->forceDelete();
    }

    public function test_view_staff() {
        dump('view_delete_staff');
        $user = \App\Models\User::find(2);
        $record = \App\Models\Staff::create(factory(\App\Models\Staff::class)->make()->toArray());
        $row = factory(\App\Models\Staff::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/staff/view/' . $record->id)
                ->assertStatus(200);
        $record->forceDelete();
    }

}
