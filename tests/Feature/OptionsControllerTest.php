<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class OptionsControllerTest extends TestCase {

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_list_options() {
        dump('test_list_options');
        $user = \App\Models\User::find(2);
        $latest = \App\Models\Option::orderBy('id', 'desc')->first();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/options')
                ->assertStatus(200)
                ->assertSee('options');
    }

    public function test_create_options() {
        dump('test_create_options');
        $user = \App\Models\User::find(2);
        $row = factory(\App\Models\Option::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->post('en/options/create', $row->toArray());
        $latest = \App\Models\Option::orderBy('id', 'desc')->first();
        $this->assertEquals($row->title, $latest->title);
        $latest->forceDelete();
    }

    public function test_edit_options() {
        dump('test_edit_options');
        $user = \App\Models\User::find(2);
        $record = \App\Models\Option::create(factory(\App\Models\Option::class)->make()->toArray());
        $row = factory(\App\Models\Option::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->post('en/options/edit/' . $record->id, $row->toArray());
        $record = \App\Models\Option::find($record->id);
        $this->assertEquals($record->title, $row->title);
        $record->forceDelete();
    }

    public function test_delete_options() {
        dump('test_delete_options');
        $user = \App\Models\User::find(2);
        $record = \App\Models\Option::create(factory(\App\Models\Option::class)->make()->toArray());
        $row = factory(\App\Models\Option::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/options/delete/' . $record->id);
        $this->assertEquals('a', 'a');
        $record->forceDelete();
    }

}
