<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ImagesControllerTest extends TestCase {

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_list_images() {
        dump('test_list_images');
        $user = \App\Models\User::find(2);
        $latest = \App\Models\Image::orderBy('id', 'desc')->first();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/images')
                ->assertStatus(200)
                ->assertSee('images');
    }

    public function test_create_images() {
        dump('test_create_images');
        $user = \App\Models\User::find(2);
        $row = factory(\App\Models\Image::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->post('en/images/create', $row->toArray());
        $latest = \App\Models\Image::orderBy('id', 'desc')->first();
        $this->assertEquals($row->title, $latest->title);
        $latest->forceDelete();
    }

    public function test_edit_images() {
        dump('test_edit_images');
        $user = \App\Models\User::find(2);
        $record = \App\Models\Image::create(factory(\App\Models\Image::class)->make()->toArray());
        $row = factory(\App\Models\Image::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->post('en/images/edit/' . $record->id, $row->toArray());
        $record = \App\Models\Image::find($record->id);
        $this->assertEquals($record->title, $row->title);
        $record->forceDelete();
    }

    public function test_delete_images() {
        dump('test_delete_images');
        $user = \App\Models\User::find(2);
        $record = \App\Models\Image::create(factory(\App\Models\Image::class)->make()->toArray());
        $row = factory(\App\Models\Image::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/images/delete/' . $record->id);
        $this->assertEquals('a', 'a');
        $record->forceDelete();
    }

    public function test_view_images() {
        dump('view_delete_images');
        $user = \App\Models\User::find(2);
        $record = \App\Models\Image::create(factory(\App\Models\Image::class)->make()->toArray());
        $row = factory(\App\Models\Image::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/images/view/' . $record->id)
                ->assertStatus(200);
        $record->forceDelete();
    }

}
