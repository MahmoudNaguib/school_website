<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class GalleryControllerTest extends TestCase {

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_list_gallery() {
        dump('test_list_gallery');
        $user = \App\Models\User::find(2);
        $latest = \App\Models\Gallery::orderBy('id', 'desc')->first();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/gallery')
                ->assertStatus(200)
                ->assertSee('gallery');
    }

    public function test_create_gallery() {
        dump('test_create_gallery');
        $user = \App\Models\User::find(2);
        $row = factory(\App\Models\Gallery::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->post('en/gallery/create', $row->toArray());
        $latest = \App\Models\Gallery::orderBy('id', 'desc')->first();
        $this->assertEquals($row->title, $latest->title);
        $latest->forceDelete();
    }

    public function test_edit_gallery() {
        dump('test_edit_gallery');
        $user = \App\Models\User::find(2);
        $record = \App\Models\Gallery::create(factory(\App\Models\Gallery::class)->make()->toArray());
        $row = factory(\App\Models\Gallery::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->post('en/gallery/edit/' . $record->id, $row->toArray());
        $record = \App\Models\Gallery::find($record->id);
        $this->assertEquals($record->title, $row->title);
        $record->forceDelete();
    }

    public function test_delete_gallery() {
        dump('test_delete_gallery');
        $user = \App\Models\User::find(2);
        $record = \App\Models\Gallery::create(factory(\App\Models\Gallery::class)->make()->toArray());
        $row = factory(\App\Models\Gallery::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/gallery/delete/' . $record->id);
        $this->assertEquals('a', 'a');
        $record->forceDelete();
    }

    public function test_view_gallery() {
        dump('view_delete_gallery');
        $user = \App\Models\User::find(2);
        $record = \App\Models\Gallery::create(factory(\App\Models\Gallery::class)->make()->toArray());
        $row = factory(\App\Models\Gallery::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/gallery/view/' . $record->id)
                ->assertStatus(200);
        $record->forceDelete();
    }

}
