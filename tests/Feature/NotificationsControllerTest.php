<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class NotificationsControllerTest extends TestCase {

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_list_notifications() {
        dump('test_list_notifications');
        $user = \App\Models\User::find(2);
        $latest = \App\Models\Notification::orderBy('id', 'desc')->first();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/notifications')
                ->assertStatus(200)
                ->assertSee('notifications');
    }

    public function test_to_notifications() {
        dump('test_to_notifications');
        $user = \App\Models\User::find(2);
        $row = \App\Models\Notification::create(factory(\App\Models\Notification::class)->make()->toArray());
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->post('en/notifications/to/' . $row->id);
        $latest = \App\Models\Notification::orderBy('id', 'desc')->first();
        $this->assertEquals($row->from_id, $latest->from_id);
        $latest->forceDelete();
    }

    public function test_read_all_notifications() {
        dump('test_read_all_notifications');
        $user = \App\Models\User::find(2);
        $to_user = \App\Models\User::create(factory(\App\Models\User::class)->make()->toArray());
        $row = \App\Models\Notification::create(factory(\App\Models\Notification::class)->make([
                            'to_id' => $to_user->id,
                        ])->toArray());
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/notifications/read-all/' . $to_user->id);
        $latest = \App\Models\Notification::orderBy('id', 'desc')->first();
        $this->assertNotNull($latest->seen_at);
        $to_user->forceDelete();
        $latest->forceDelete();
    }

    public function test_view_notifications() {
        dump('test_view_notifications');
        $user = \App\Models\User::find(2);
        $record = \App\Models\Notification::create(factory(\App\Models\Notification::class)->make()->toArray());
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/notifications/view/' . $record->id);
        $latest = \App\Models\Notification::orderBy('id', 'desc')->first();
        $this->assertEquals($record->child_id, $latest->child_id);
        $latest->forceDelete();
    }

    public function test_delete_notifications() {
        dump('test_delete_notifications');
        $user = \App\Models\User::find(2);
        $record = \App\Models\Notification::create(factory(\App\Models\Notification::class)->make()->toArray());
        $row = factory(\App\Models\Notification::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/notifications/delete/' . $record->id);
        $this->assertEquals('a', 'a');
        $record->forceDelete();
    }

}
