<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class ConfigsControllerTest extends TestCase {

   

    public function test_edit_configs() {
        dump('test_edit_configs');
        $user = \App\Models\User::find(2);
        $record = \App\Models\Config::create(factory(\App\Models\Config::class)->make()->toArray());
        $row = factory(\App\Models\Config::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->post('en/configs/edit/' . $record->id, $row->toArray());
        $record = \App\Models\Config::find($record->id);
        $this->assertEquals($record->title, $row->title);
        $record->forceDelete();
    }

}
