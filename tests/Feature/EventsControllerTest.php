<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class EventsControllerTest extends TestCase {

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_list_events() {
        dump('test_list_events');
        $user = \App\Models\User::find(2);
        $latest = \App\Models\Event::orderBy('id', 'desc')->first();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/events')
                ->assertStatus(200)
                ->assertSee('events');
    }

    public function test_create_events() {
        dump('test_create_events');
        $user = \App\Models\User::find(2);
        $row = factory(\App\Models\Event::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->post('en/events/create', $row->toArray());
        $latest = \App\Models\Event::orderBy('id', 'desc')->first();
        $this->assertEquals($row->title, $latest->title);
        $latest->forceDelete();
    }

    public function test_edit_events() {
        dump('test_edit_events');
        $user = \App\Models\User::find(2);
        $record = \App\Models\Event::create(factory(\App\Models\Event::class)->make()->toArray());
        $row = factory(\App\Models\Event::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->post('en/events/edit/' . $record->id, $row->toArray());
        $record = \App\Models\Event::find($record->id);
        $this->assertEquals($record->title, $row->title);
        $record->forceDelete();
    }

    public function test_delete_events() {
        dump('test_delete_events');
        $user = \App\Models\User::find(2);
        $record = \App\Models\Event::create(factory(\App\Models\Event::class)->make()->toArray());
        $row = factory(\App\Models\Event::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/events/delete/' . $record->id);
        $this->assertEquals('a', 'a');
        $record->forceDelete();
    }

    public function test_view_events() {
        dump('view_delete_events');
        $user = \App\Models\User::find(2);
        $record = \App\Models\Event::create(factory(\App\Models\Event::class)->make()->toArray());
        $row = factory(\App\Models\Event::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/events/view/' . $record->id)
                ->assertStatus(200);
        $record->forceDelete();
    }

}
