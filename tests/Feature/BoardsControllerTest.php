<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class BoardsControllerTest extends TestCase {

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_list_boards() {
        dump('test_list_boards');
        $user = \App\Models\User::find(2);
        $latest = \App\Models\Board::orderBy('id', 'desc')->first();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/boards')
                ->assertStatus(200)
                ->assertSee('boards');
    }

    public function test_create_boards() {
        dump('test_create_boards');
        $user = \App\Models\User::find(2);
        $row = factory(\App\Models\Board::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->post('en/boards/create', $row->toArray());
        $latest = \App\Models\Board::orderBy('id', 'desc')->first();
        $this->assertEquals($row->title, $latest->title);
        $latest->forceDelete();
    }

    public function test_edit_boards() {
        dump('test_edit_boards');
        $user = \App\Models\User::find(2);
        $record = \App\Models\Board::create(factory(\App\Models\Board::class)->make()->toArray());
        $row = factory(\App\Models\Board::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->post('en/boards/edit/' . $record->id, $row->toArray());
        $record = \App\Models\Board::find($record->id);
        $this->assertEquals($record->title, $row->title);
        $record->forceDelete();
    }

    public function test_delete_boards() {
        dump('test_delete_boards');
        $user = \App\Models\User::find(2);
        $record = \App\Models\Board::create(factory(\App\Models\Board::class)->make()->toArray());
        $row = factory(\App\Models\Board::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/boards/delete/' . $record->id);
        $this->assertEquals('a', 'a');
        $record->forceDelete();
    }

    public function test_view_boards() {
        dump('view_delete_boards');
        $user = \App\Models\User::find(2);
        $record = \App\Models\Board::create(factory(\App\Models\Board::class)->make()->toArray());
        $row = factory(\App\Models\Board::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/boards/view/' . $record->id)
                ->assertStatus(200);
        $record->forceDelete();
    }

}
