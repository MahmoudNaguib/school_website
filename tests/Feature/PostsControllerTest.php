<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class PostsControllerTest extends TestCase {

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_list_posts() {
        dump('test_list_posts');
        $user = \App\Models\User::find(2);
        $latest = \App\Models\Post::orderBy('id', 'desc')->first();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/posts')
                ->assertStatus(200)
                ->assertSee('posts');
    }

    public function test_create_posts() {
        dump('test_create_posts');
        $user = \App\Models\User::find(2);
        $row = factory(\App\Models\Post::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->post('en/posts/create', $row->toArray());
        $latest = \App\Models\Post::orderBy('id', 'desc')->first();
        $this->assertEquals($row->title, $latest->title);
        $latest->forceDelete();
    }

    public function test_edit_posts() {
        dump('test_edit_posts');
        $user = \App\Models\User::find(2);
        $record = \App\Models\Post::create(factory(\App\Models\Post::class)->make()->toArray());
        $row = factory(\App\Models\Post::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->post('en/posts/edit/' . $record->id, $row->toArray());
        $record = \App\Models\Post::find($record->id);
        $this->assertEquals($record->title, $row->title);
        $record->forceDelete();
    }

    public function test_delete_posts() {
        dump('test_delete_posts');
        $user = \App\Models\User::find(2);
        $record = \App\Models\Post::create(factory(\App\Models\Post::class)->make()->toArray());
        $row = factory(\App\Models\Post::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/posts/delete/' . $record->id);
        $this->assertEquals('a', 'a');
        $record->forceDelete();
    }

    public function test_view_posts() {
        dump('view_delete_posts');
        $user = \App\Models\User::find(2);
        $record = \App\Models\Post::create(factory(\App\Models\Post::class)->make()->toArray());
        $row = factory(\App\Models\Post::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/posts/view/' . $record->id)
                ->assertStatus(200);
        $record->forceDelete();
    }

}
