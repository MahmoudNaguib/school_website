<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class JobTitlesControllerTest extends TestCase {

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_list_job_titles() {
        dump('test_list_job_titles');
        $user = \App\Models\User::find(2);
        $latest = \App\Models\JobTitle::orderBy('id', 'desc')->first();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/job_titles')
                ->assertStatus(200)
                ->assertSee('job_titles');
    }

    public function test_create_job_titles() {
        dump('test_create_job_titles');
        $user = \App\Models\User::find(2);
        $row = factory(\App\Models\JobTitle::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->post('en/job_titles/create', $row->toArray());
        $latest = \App\Models\JobTitle::orderBy('id', 'desc')->first();
        $this->assertEquals($row->title, $latest->title);
        $latest->forceDelete();
    }

    public function test_edit_job_titles() {
        dump('test_edit_job_titles');
        $user = \App\Models\User::find(2);
        $record = \App\Models\JobTitle::create(factory(\App\Models\JobTitle::class)->make()->toArray());
        $row = factory(\App\Models\JobTitle::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->post('en/job_titles/edit/' . $record->id, $row->toArray());
        $record = \App\Models\JobTitle::find($record->id);
        $this->assertEquals($record->title, $row->title);
        $record->forceDelete();
    }

    public function test_delete_job_titles() {
        dump('test_delete_job_titles');
        $user = \App\Models\User::find(2);
        $record = \App\Models\JobTitle::create(factory(\App\Models\JobTitle::class)->make()->toArray());
        $row = factory(\App\Models\JobTitle::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/job_titles/delete/' . $record->id);
        $this->assertEquals('a', 'a');
        $record->forceDelete();
    }

    public function test_view_job_titles() {
        dump('view_delete_job_titles');
        $user = \App\Models\User::find(2);
        $record = \App\Models\JobTitle::create(factory(\App\Models\JobTitle::class)->make()->toArray());
        $row = factory(\App\Models\JobTitle::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/job_titles/view/' . $record->id)
                ->assertStatus(200);
        $record->forceDelete();
    }

}
