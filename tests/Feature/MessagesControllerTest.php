<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class MessagesControllerTest extends TestCase {

    /**
     * A basic test example.
     *
     * @return void
     */
    public function test_list_messages() {
        dump('test_list_messages');
        $user = \App\Models\User::find(2);
        $latest = \App\Models\Message::orderBy('id', 'desc')->first();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/messages')
                ->assertStatus(200)
                ->assertSee('messages');
    }

    public function test_delete_messages() {
        dump('test_delete_messages');
        $user = \App\Models\User::find(2);
        $record = \App\Models\Message::create(factory(\App\Models\Message::class)->make()->toArray());
        $row = factory(\App\Models\Message::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/messages/delete/' . $record->id);
        $this->assertEquals('a', 'a');
        $record->forceDelete();
    }

    public function test_view_messages() {
        dump('view_delete_messages');
        $user = \App\Models\User::find(2);
        $record = \App\Models\Message::create(factory(\App\Models\Message::class)->make()->toArray());
        $row = factory(\App\Models\Message::class)->make();
        $response = $this->actingAs($user)
                ->withSession(['locale' => 'en'])
                ->get('en/messages/view/' . $record->id)
                ->assertStatus(200);
        $record->forceDelete();
    }

}
