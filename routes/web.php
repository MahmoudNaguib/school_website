<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */

/* Route::get('/', function () {
  return view('welcome');
  });
 */
Route::group(['prefix' => LaravelLocalization::setLocale() , 'middleware' => [ 'localeSessionRedirect', 'localizationRedirect', 'localeViewPath']], function() {
    AdvancedRoute::controller('auth', 'AuthController');
    Route::group(['middleware' => ['auth']], function() {
        AdvancedRoute::controller('profile', 'ProfileController');
        AdvancedRoute::controller('notifications', 'NotificationsController');
        Route::group(['middleware' => ['isAdmin'], 'prefix' => 'admin'], function() {
            AdvancedRoute::controller('roles', 'Admin\RolesController');
            AdvancedRoute::controller('users', 'Admin\UsersController');
            AdvancedRoute::controller('posts', 'Admin\PostsController');
            AdvancedRoute::controller('events', 'Admin\EventsController');
            AdvancedRoute::controller('gallery', 'Admin\GalleryController');
            AdvancedRoute::controller('images', 'Admin\ImagesController');
            AdvancedRoute::controller('job_titles', 'Admin\JobTitlesController');
            AdvancedRoute::controller('staff', 'Admin\StaffController');
            AdvancedRoute::controller('messages', 'Admin\MessagesController');
            AdvancedRoute::controller('boards', 'Admin\BoardsController');
            AdvancedRoute::controller('configs', 'Admin\ConfigsController');
            AdvancedRoute::controller('search', 'Admin\SearchController');
            AdvancedRoute::controller('translator', 'Admin\TranslatorController');
            AdvancedRoute::controller('options', 'Admin\OptionsController');
            AdvancedRoute::controller('ajax', 'Admin\AjaxController');
            AdvancedRoute::controller('/', 'Admin\DashBoardController');
        });
    });
    AdvancedRoute::controller('/posts', 'PostsController');
    AdvancedRoute::controller('/gallery', 'GalleryController');
    AdvancedRoute::controller('/events', 'EventsController');
    AdvancedRoute::controller('/staff', 'StaffController');
    AdvancedRoute::controller('/boards', 'BoardsController');
    AdvancedRoute::controller('/messages', 'MessagesController');
    Route::get('/about', 'PagesController@about');
    Route::get('/contact', 'PagesController@contact');
    AdvancedRoute::controller('/', 'HomeController');
});


Route::prefix('api')->group(function () {
    require_once __DIR__ . '/api.php';
});
